FROM node:alpine as builder
WORKDIR /app
COPY ./package*.json ./
# RUN npm install --force
COPY ./ ./
# RUN npm run build

FROM nginx:alpine
COPY --from=builder /app/dist/coreui-free-angular-admin-template /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
