export const environment = {
  localEndpoint:"http://10.2.5.21:1323",
  apiEndpoint:"http://10.2.2.59:8089",
  middleWareEndpoint:"http://10.2.2.64:8091/middleware",
  jsonDataTestEndpoint:"http://localhost:3000",
  production: true
};
