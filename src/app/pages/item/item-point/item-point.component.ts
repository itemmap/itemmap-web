import { MapService } from 'src/app/service/map.service';
import { ItemCategoryVO } from 'src/app/model/itemCategoryVO';
import { ItemService } from './../../../service/item.service';
import { AfterViewInit, Component,  ElementRef,  HostListener,  OnInit, ViewChild  } from '@angular/core';
import { GroupFloorPointItemVO } from 'src/app/model/groupFloorPointItemVO';
import { MapInfoVO } from 'src/app/model/mapInfoVO';
import { UserGroupInfoVO } from 'src/app/model/userGroupInfoVO';
import { AccountService } from 'src/app/service/account.service';
import { GroupVO } from 'src/app/model/groupVO';
import { GroupItemInfoVO } from 'src/app/model/GroupItemInfoVO';
import { NeedNumberItemVO } from 'src/app/model/needNumberItemVO';
import { NoNumberItemVO } from 'src/app/model/noNumberItemVO';
import { ItemPointVO } from 'src/app/model/itemPointVO';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from '../../shared/dialog/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ItemVO } from 'src/app/model/itemVO';
import { GroupService } from 'src/app/service/group.service';




@Component({
  selector: 'app-item-point',
  templateUrl: './item-point.component.html',
  styleUrls: ['./item-point.component.scss']
})
export class ItemPointComponent  implements OnInit{
  itemCategoryList!: ItemCategoryVO[];
  floorPointItemList:GroupFloorPointItemVO[] = [];
  groupMapList!:MapInfoVO[];
  groupList!: GroupVO[];
  selectedGroup!:GroupVO;
  selectedMap!:MapInfoVO;
  userAccount!: string;
  userGroupInfo!: UserGroupInfoVO;
  groupItem!:GroupItemInfoVO;  //群組已經打點的物品
  isUserNoGroup:boolean = false;
  @ViewChild('countInput') countInput!: ElementRef;
  @ViewChild('canvas')
  canvas!: ElementRef<HTMLCanvasElement>;
  ctx!: CanvasRenderingContext2D;
  controlPanelVisible:boolean = false;
  groupCanPointItem!:GroupItemInfoVO;
  isGroupNoItemCanPoint:boolean = false;
  selectedNoUuidItem!:NoNumberItemVO;
  isSelectedNoUuidItem:boolean = false;
  itemPointVO:ItemPointVO = {
    groupid: 0,
    floor_id: 0,
    x: '',
    y: '',
    item_number_id_list: [],
    item_group_id: 0,
    count: 0
  };
  displayedColumnsGroup: string[] = ['category','name','itemUuid', 'count','action'];
  pointedItem = new MatTableDataSource(this.floorPointItemList);
  originGroupUuidItemList!:NeedNumberItemVO[];
  originGroupNoUuidItemList!:NoNumberItemVO[];
  pointedUuidItem!:NoNumberItemVO;
  selectedPointIconList:any = [];
  selectedPointIcon:any;
  groupNoMap:boolean = false;
  allItemList!:ItemVO[];
  isAdminPointItem:boolean = false;
  userRole:string = ''
  isAdmin:boolean = false;
  userAdminGroupId!:number;
  isInSelfGroup:boolean =false;







  constructor(private groupService:GroupService,private itemService:ItemService,private mapService:MapService,private accountService:AccountService, public dialog: MatDialog){}

  ngOnInit(): void {
    this.fetchData();
  }


  async fetchData(){
    this.userRole =  localStorage.getItem('roleList')??""
    // console.log(this.userRole)
    if(this.userRole == 'admin'){
      this.isAdmin = true
    }
    // console.log(this.isAdmin)
    this.allItemList = await this.itemService.getAllItem();
    this.itemCategoryList = await this.itemService.getAllItemcategory();
    this.userAccount = localStorage.getItem('userAccount') ?? "";
    this.userGroupInfo = await this.accountService.getAccountAllGroupList(this.userAccount);
    if(this.userGroupInfo == null){
      this.isUserNoGroup = true;
    }
    this.groupList = this.userGroupInfo.group_info;
    //是管理員的groupList
    if(this.isAdmin){
      this.groupList = await this.groupService.getAllGroupList();
      this.userAdminGroupId = this.userGroupInfo.group_info.find((group)=>group.is_admin_group == 1 )?.id ?? 0;
      // console.log(this.groupList)
      // console.log(this.userAdminGroupId)
      this.groupList = this.groupList.filter((group)=>group.is_admin_group !==1)
    }
    this.selectedGroup = this.groupList[0];
    await this.selectGroup();
    await this.initDrawPointedItem();

  }



   checkUserInGroup():boolean{
// console.log(this.selectedGroup.id);

      var iresult:boolean = false;
      this.userGroupInfo.group_info.forEach((groupitem)=>{
        // console.log(groupitem.id);
      if(groupitem.id == this.selectedGroup.id){
        iresult = true;
      }
        }
      )

    return iresult
  }


  async selectGroup(){

    this.groupNoMap = false;
    this.groupMapList = await this.mapService.getGroupMapInfoList(this.selectedGroup.id);
    if(this.groupMapList.length == 0){
      // console.log('目前沒有負責的大樓地圖');
      this.groupNoMap = true;
      this.selectMap();
      return;
    }
    this.selectedMap = this.groupMapList[0];
    // console.log(this.selectedGroup)
    this.isInSelfGroup =  this.checkUserInGroup()
    // console.log(this.isInSelfGroup)
    this.selectMap();
  }


  async findGroupMapItem(){
    this.groupItem = await this.mapService.findGroupItem(this.selectedGroup.id);
    // console.log(this.groupItem)
    this.originGroupUuidItemList = this.groupItem.item_number_list;
    this.originGroupNoUuidItemList = this.groupItem.item_distribute_group_list;
  }





  async selectMap(){
    if(!this.groupNoMap){
    this.itemPointVO.floor_id = this.selectedMap.floor_id;
    this.itemPointVO.groupid = this.selectedGroup.id;
    this.floorPointItemList = await this.mapService.getFloorMapPointInfo(this.selectedMap);
    console.log(this.floorPointItemList)
    this.pointedItem = new MatTableDataSource(this.floorPointItemList);
    await this.findGroupMapItem();
    this.changeMapDrawPointedItem();
    }
    this.controlPanelVisible = false

  }


  selectPointItemType(){
    this.getGroupCanPointItem();
    this.itemPointVO.count = 0;
    this.itemPointVO.item_group_id = 0;
    this.itemPointVO.item_number_id_list = [];
    this.selectedPointIconList = [];
    this.selectedPointIcon = '';
  }


   checkNumberItemPointPermission(groupNumberItem:NeedNumberItemVO){

    const mappingObject = this.allItemList.find((item)=>item.id==groupNumberItem.item_id) as ItemVO
    if(mappingObject.is_map_point === 0 ){
      return true;
    }
    return false;

}




checkNoUuidItemRemovePermission(removeItem:GroupFloorPointItemVO){
var result = false;
  //沒有編號的物品

    let noUuidItem = this.originGroupNoUuidItemList.filter((element) => element.item_group_id === removeItem.item_group_id)[0] as NoNumberItemVO;
    const mappingObject = this.allItemList.find((item)=>item.id == noUuidItem.item_id)
   if(mappingObject?.is_map_point == 1){
    result = true
   }

  return result;

}


checkUuidItemRemovePermission(removeItem:GroupFloorPointItemVO){
  var result = false;
  //沒有編號的物品

    let uuidItem = this.originGroupUuidItemList.filter((element) => element.item_number_id === removeItem.item_number_id)[0] as NeedNumberItemVO;
    const mappingObject = this.allItemList.find((item)=>item.id == uuidItem.item_id)
   if(mappingObject?.is_map_point == 1){
    result = true
   }

  return result;
}



  async selecteduuidItem(event:any,uuiditem:NeedNumberItemVO){
    //勾選checkbox
    if(event.checked == true ){
    this.itemPointVO.item_number_id_list.push(uuiditem.item_number_id)
    if ( !this.selectedPointIconList.includes(uuiditem.icon)){
    this.selectedPointIconList.push(uuiditem.icon)
    }
  }
    //取消勾選checkbox
    if(event.checked == false ){
      const index = this.itemPointVO.item_number_id_list.findIndex(x => x === uuiditem.item_number_id);
      this.itemPointVO.item_number_id_list.splice(index,1);
      this.selectedPointIconList.splice(index,1);
      }
      this.itemPointVO.count = this.itemPointVO.item_number_id_list.length;

  }







async changeMapDrawPointedItem(){
  const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
  this.ctx = canvasEl.getContext('2d') as CanvasRenderingContext2D;
  const context = canvasEl.getContext('2d');
  // canvasEl.width =  window.innerWidth /2;
  // canvasEl.height = 600;
  const img = new Image();
  if(!this.groupNoMap){
  img.src = this.selectedMap.floor_Image_url;
  // console.log( img)
  img.onload = () => {
    canvasEl.width = img.width /8;
    canvasEl.height = img.height/5;
    context!.drawImage(img, 0, 0, canvasEl.width, canvasEl.height);
    //繪製已經打點的物品
    for (const point of this.floorPointItemList) {
      this.drawItemInfo(Number(point.point_x), Number(point.point_y), point);

    }
  };
}

}




async  drawItemInfo(x: string | number, y: string | number, item: GroupFloorPointItemVO) {
  const iconImage = new Image();
  const icon = await this.findItemIcon(item);
  if(icon){
  iconImage.src = icon ??""
          iconImage.onload = () => {
          this.ctx.beginPath();
          this.ctx.arc(Number(x), Number(y), 10, 0, 2 * Math.PI);
          this.ctx.fill();
          this.ctx.drawImage(iconImage, Number(x) - 10, Number(y) - 10, 30, 30);
        }
      }
}







 async initDrawPointedItem(){
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.ctx = canvasEl.getContext('2d') as CanvasRenderingContext2D;
    const context = canvasEl.getContext('2d');
    // canvasEl.width =  window.innerWidth /2;
    // canvasEl.height = window.innerHeight /2;
    const img = new Image();
    if(!this.groupNoMap){
    img.src = await this.selectedMap.floor_Image_url;

    if(img.src != ''){
    img.onload = () => {
      canvasEl.width = img.width /8;
      canvasEl.height = img.height/5;
      // console.log(img.width)
      // console.log(img.height)
      context!.drawImage(img, 0, 0, canvasEl.width, canvasEl.height);
      //繪製已經打點的物品
      for (const point of this.floorPointItemList) {
        this.initDrawItemInfo(Number(point.point_x), Number(point.point_y), point);

      }
    };

  }
 }
//監聽畫布上的icon
const infoElement = document.createElement('div');
infoElement.style.position = 'fixed';
infoElement.style.color = 'gray';
infoElement.style.fontSize = '25px';
infoElement.style.fontWeight = 'bold';
infoElement.style.backgroundColor = 'white';
document.body.appendChild(infoElement);
canvasEl.addEventListener('mousemove', async (event) => {
  const rect = canvasEl.getBoundingClientRect(); // 取得 canvas 元素相對於視窗的位置
  const scaleX = canvasEl.width / rect.width; // 計算 x 軸的縮放比例
  const scaleY = canvasEl.height / rect.height; // 計算 y 軸的縮放比例
  const mouseX = (event.clientX - rect.left) * scaleX; // 轉換滑鼠 x 座標
  const mouseY = (event.clientY - rect.top) * scaleY;
  infoElement.style.top = `${event.clientY}px`;
  infoElement.style.left = `${event.clientX+30}px`;
  let infoText = '';
  if(!this.groupNoMap){
  for (const point of this.floorPointItemList) {
    // 計算滑鼠位置與打點位置之間的距離
    const distance = Math.sqrt((mouseX - Number(point.point_x)) ** 2 + (mouseY - Number(point.point_y)) ** 2);
    if (distance < 30) {
      //沒有編號的物品
      if(point.item_number_id == ''){
        const item = this.originGroupNoUuidItemList.filter((element) => element.item_group_id === point.item_group_id)[0]as NoNumberItemVO;
        infoText += `${ item.item_name} * ${point.count}`;

      }
       //有編號的物品
      if(point.item_number_id !== ''){
        const filteredItems = this.originGroupUuidItemList.filter((element) => element.item_number_id === point.item_number_id);
        if (filteredItems.length > 0) {
          const item = filteredItems[0] as NeedNumberItemVO;
          infoText += `${item.item_name} <br>${point.item_number_id}`;
        } else {
        }
      }

    }
    if (infoText.length > 0) {
      infoElement.innerHTML = infoText;
    } else {
      infoElement.textContent = '';
    }
  }
}})

}



async  initDrawItemInfo(x: string | number, y: string | number, item: GroupFloorPointItemVO) {
  const iconImage = new Image();
  const icon = await this.findItemIcon(item);
  if(icon){
  iconImage.src = icon ??""
          iconImage.onload = () => {
          this.ctx.beginPath();
          this.ctx.arc(Number(x), Number(y), 10, 0, 2 * Math.PI);
          this.ctx.fill();
          this.ctx.drawImage(iconImage, Number(x) - 10, Number(y) - 10, 30, 30);
        }
  }
}


async findItemIcon(item:any){
  let icon = '';
  if(item.item_group_id == 0){
    let uuidItem =  await this.groupItem.item_number_list.filter((element) => element.item_number_id === item.item_number_id)[0] as NeedNumberItemVO;
    icon = `${uuidItem.icon}`
    return icon;
  }
  //沒有編號的物品
  if(item.item_group_id !== 0){
    const noUuidItems =await this.groupItem.item_distribute_group_list?.filter((element) => element.item_group_id === item.item_group_id) as NoNumberItemVO[];
    const noUuidItem = noUuidItems?.[0];
    icon = noUuidItem?.icon ?? '';
  }
  return icon;
}



openControlPanel(){
  this.controlPanelVisible = !this.controlPanelVisible;
  if(this.controlPanelVisible){
  this.getGroupCanPointItem();
  }
}


async getGroupCanPointItem(){
 this.groupCanPointItem = await this.mapService.findGroupItem(this.selectedGroup.id);
 if(this.groupCanPointItem.item_distribute_group_list.length === 0 &&  this.groupCanPointItem.item_number_list.length === 0){
  this.isGroupNoItemCanPoint = true;
 }
await this.filterPointedItem();
}



async filterPointedItem(){
  //從群組可打點物品清單過濾已經打點的有編號物品
this.groupCanPointItem.item_number_list = this.groupCanPointItem.item_number_list.filter(item => {
  return !this.floorPointItemList.some(floorItem => floorItem.item_number_id == item.item_number_id);
});

}



decresecount(groupid : number){
  var count: number =0;
  this.floorPointItemList.forEach(item => {
      if(item.item_group_id === groupid){
        count += item.count
      }
  });
  return count;
}



 checkNoNumberItemPointPermission(noUuiditemOption: NoNumberItemVO){

  const mappingObject = this.allItemList.find((item)=>item.id == noUuiditemOption.item_id)
  //不可看到的物品
 if(!this.isAdmin){
  if(mappingObject?.is_map_point !== 0 ){
    return false;
  }
}
  // 可看到的物品
  return true;
}

 AdmincheckNoNumberItemPointPermission(noUuiditemOption: NoNumberItemVO){
  const mappingObject = this.allItemList.find((item)=>item.id == noUuiditemOption.item_id);
  if(this.isAdmin && mappingObject?.admin_group_id == this.userAdminGroupId && mappingObject.is_map_point == 0){
    return true;
  }
 //不是admin
  if(this.isInSelfGroup && mappingObject?.is_map_point == 1){
    return true
  }


  return false;


}

AdmincheckRemoveNoNumberItemPointPermission(noUuiditemOption: GroupFloorPointItemVO){
  const object = this.originGroupNoUuidItemList.find((i)=>i.id == noUuiditemOption.item_group_id)
  // console.log('lin488',object)
  const converobject =   this.allItemList.find((g)=>g.id == object?.item_id )
  // console.log('line490',converobject)
  if(this.isAdmin && converobject?.admin_group_id == this.userAdminGroupId && converobject.is_map_point == 0){
    return true;
  }
  if(this.isInSelfGroup && converobject?.is_map_point == 1){
    return true
  }
  return false;
}




AdmincheckRemoveNumberItemPointPermission(noUuiditemOption: GroupFloorPointItemVO){
  const object = this.originGroupUuidItemList.find((i)=>i.item_number_id == noUuiditemOption.item_number_id)
  // console.log(object)
  // console.log('lin488',object)
  const converobject =   this.allItemList.find((g)=>g.id == object?.item_id )
  // console.log('line490',converobject)
  if(this.isAdmin && converobject?.admin_group_id == this.userAdminGroupId && converobject.is_map_point == 0){
    return true;
  }
  if(this.isInSelfGroup && converobject?.is_map_point == 1){
    return true
  }
  return false;
}



admincheckNumberItemPointPermission(item:NeedNumberItemVO){
//  console.log(item)
 const object = this.originGroupUuidItemList.find((i)=>i.item_number_id == item.item_number_id)
 const convertobject = this.allItemList.find((g)=>g.id == object?.item_id )
//  console.log(convertobject)
 if(this.isAdmin && convertobject?.admin_group_id == this.userAdminGroupId && convertobject.is_map_point == 0){
  return true;
}
if(this.isInSelfGroup && convertobject?.is_map_point == 1){
  return true
}
return false;

}




 async adminSelectNoUuidItem(noUuiditemOption: NoNumberItemVO){

  this.isAdminPointItem = false;
  this.isSelectedNoUuidItem = true;
  this.selectedNoUuidItem = noUuiditemOption;
  var isOtherAdminGroupItem =  await this.AdmincheckNoNumberItemPointPermission(noUuiditemOption);
  console.log(isOtherAdminGroupItem)
  if(isOtherAdminGroupItem && !this.isAdmin){
    this.selectedNoUuidItem = noUuiditemOption;
    this.isAdminPointItem = true;
    this.isSelectedNoUuidItem =false;
    return;
  }

  this.selectedNoUuidItem = noUuiditemOption;
  this.itemPointVO.item_group_id = noUuiditemOption.item_group_id;
  this.selectedPointIcon = noUuiditemOption.icon;

}


async selectNoUuidItem(noUuiditemOption: NoNumberItemVO){
  this.isAdminPointItem = false;
  this.isSelectedNoUuidItem = true;
  var isAdminPointItem =  await this.checkNoNumberItemPointPermission(noUuiditemOption);
  // console.log(isAdminPointItem)
  if(isAdminPointItem && !this.isAdmin){
    this.selectedNoUuidItem = noUuiditemOption;
    this.isAdminPointItem = true;
    this.isSelectedNoUuidItem =false;
    return;
  }

  this.selectedNoUuidItem = noUuiditemOption;
  this.itemPointVO.item_group_id = noUuiditemOption.item_group_id;
  this.selectedPointIcon = noUuiditemOption.icon;
}



checkAdminRemoveUuidPermission(item:GroupFloorPointItemVO){
  var result = false;
  //沒有編號的物品

    let uuidItem = this.originGroupUuidItemList.filter((element) => element.item_number_id === item.item_number_id)[0] as NeedNumberItemVO;
    const mappingObject = this.allItemList.find((item)=>item.id == uuidItem.item_id)
   if(mappingObject?.is_map_point == 0){
    result = true
   }

  return result;
}





mappingItemName(item:GroupFloorPointItemVO){
  let itemText = ''
  //有編號的物品    item_group_id = 0
  if(item.item_group_id == 0){
    let uuidItem = this.originGroupUuidItemList.filter((element) => element.item_number_id === item.item_number_id)[0] as NeedNumberItemVO;
    itemText = `${uuidItem.item_name}`
  }
  //沒有編號的物品
  if(item.item_group_id !== 0){
    let noUuidItem = this.originGroupNoUuidItemList.filter((element) => element.item_group_id === item.item_group_id)[0] as NoNumberItemVO;
    itemText = `${noUuidItem.item_name}`
  }
  return itemText;
}


mappingItemIcon(item:any){
  let icon = ''
  //有編號的物品    item_group_id = 0
  if(item.item_group_id == 0){
    let uuidItem = this.originGroupUuidItemList.filter((element) => element.item_number_id === item.item_number_id)[0] as NeedNumberItemVO;
    icon = `${uuidItem.icon}`
  }
  //沒有編號的物品
  if(item.item_group_id !== 0){
    let noUuidItem = this.originGroupNoUuidItemList.filter((element) => element.item_group_id === item.item_group_id)[0] as NoNumberItemVO;
    icon = `${noUuidItem.icon}`
  }
  return icon;
}



async pointOnMap(event:any){


  if(this.itemPointVO.count ==0){
    return;
  }
  let curleft = 0,
  curtop = 0;
  curleft += event.offsetX;
  curtop += event.offsetY;
   await this.drawItemOnMap(curleft, curtop, this.itemPointVO);



}


validedPointItemCount(item:ItemPointVO,groupid:number):boolean{
  var pointedCount: number = 0;
  var filterItem: NoNumberItemVO | undefined;
  var canPointCount:number = 0;
  filterItem = this.originGroupNoUuidItemList.find((element) => element.item_group_id == groupid);
  if (filterItem) {
    //該物品可以打點的數量
    pointedCount = this.decresecount(groupid);
    canPointCount = filterItem.count - pointedCount
    if(this.itemPointVO.count > canPointCount){
        // console.log('超過該物品可打點的數量了')
        return false
    }

  }
  return true
}




async drawItemOnMap(x: string | number, y: string | number, item: ItemPointVO){
if(item.item_number_id_list.length ==0){
  var checkResult = this.validedPointItemCount(item,item.item_group_id);
  if(!checkResult){
    this.customDialog('提醒','超過該物品可打點的數量');
    this.itemPointVO.count =0;
    return;
  }
}
  const iconImage = new Image();
     //有編號物品的情況
     if(item.item_number_id_list.length > 0){
       //先寫進db在重新繪圖
       const point = {
        x: Number(x),
        y: Number(y),
        item: item
      };
      await this.mapService.saveItemPointInfo(this.selectedGroup.id,this.selectedMap.floor_id,item,point.x,point.y);
      this.floorPointItemList = await this.mapService.getFloorMapPointInfo(this.selectedMap);
      this.pointedItem = new MatTableDataSource(this.floorPointItemList);
      for (const point of this.floorPointItemList) {
        this.drawItemInfo(Number(point.point_x), Number(point.point_y), point);

      }


    }

      //沒有編號物品的情況
     if(item.item_number_id_list.length == 0){
          iconImage.src = this.selectedPointIcon;
          iconImage.onload = () => {
          this.ctx.beginPath();
          this.ctx.arc(Number(x), Number(y), 10, 0, 2 * Math.PI);
          this.ctx.fill();
          this.ctx.drawImage(iconImage, Number(x) - 10, Number(y) - 10, 30, 30);
        }

        const point = {
          x: Number(x),
          y: Number(y),
          item: item
        };
        await this.mapService.saveItemPointInfo(this.selectedGroup.id,this.selectedMap.floor_id,item,point.x,point.y);
        this.floorPointItemList = await this.mapService.getFloorMapPointInfo(this.selectedMap);
        this.pointedItem = new MatTableDataSource(this.floorPointItemList);

      }
      await this.selectPointItemType();

}






async removeMark(item:GroupFloorPointItemVO){
 await this.mapService.deleteFloorMapPointInfo(item.id);
 this.floorPointItemList = await this.mapService.getFloorMapPointInfo(this.selectedMap);
 this.pointedItem = new MatTableDataSource(this.floorPointItemList);
 this.groupCanPointItem = await this.mapService.findGroupItem(this.selectedGroup.id);
 await this.filterPointedItem();
 await this.initDrawPointedItem()



}


customDialog(title: string, content: string) {
  let data = {
    "title": title,
    "content": content
  }
  let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
  return dialogRef;
}



}
















