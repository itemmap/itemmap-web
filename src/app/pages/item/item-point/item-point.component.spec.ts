import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemPointComponent } from './item-point.component';

describe('ItemPointComponent', () => {
  let component: ItemPointComponent;
  let fixture: ComponentFixture<ItemPointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemPointComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
