import { HttpClient } from '@angular/common/http';
import {
  Component,
  ElementRef,
  ViewChild,
  NgZone,
  OnInit
} from '@angular/core';
import { ResponseDef } from 'src/app/constant/ResponseDef';
import { ResponseModel } from 'src/app/model/responseModel';
import { AddItemCategoryVO } from 'src/app/model/addItemCategoryVO';
import { environment } from 'src/environments/environment';
import { ItemService } from 'src/app/service/item.service';
import { ItemCategoryVO } from 'src/app/model/itemCategoryVO';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'src/app/pages/shared/dialog/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { UtilsService } from 'src/app/util/utils.service';
import { CommondialogComponent } from '../../shared/dialog/commondialog/commondialog.component';

@Component({
  selector: 'app-item-category',
  templateUrl: './item-category.component.html',
  styleUrls: ['./item-category.component.scss'],
})

export class ItemCategoryComponent implements OnInit {
  filtertext:string = '';
  itemCategoryList!: ItemCategoryVO[];
  showAnotherCard: boolean = false;
  displayedColumns: string[] = ['icon', 'name', 'action'];
  dataSource = new MatTableDataSource(this.itemCategoryList);
  imageWidth: number = 0;
  imageHeight: number = 0;
  cardvisible: boolean = false;
  selectedImageSrc: string = '';
  imageTooLarge: boolean = false;
  addItemCategoryVO:AddItemCategoryVO ={
    icon: '',
    name: '',
  };


  @ViewChild('fileUpload') fileUpload!: ElementRef;

  constructor(
    private http: HttpClient,
    private ngZone: NgZone,
    private itemService: ItemService,
    public dialog: MatDialog,
    private utilsService:UtilsService
  ) {}


  ngOnInit(): void {
    this.fetchData();
  }

  async fetchData() {
    try {
      this.itemCategoryList = await this.itemService.getAllItemcategory();
      // console.log(this.itemCategoryList);
      this.dataSource = new MatTableDataSource(this.itemCategoryList);
    } catch (error) {
      console.error(error);
    }
  }


  findItemCategory(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }


  addCardBtn() {
    this.cardvisible = !this.cardvisible;
  }

  uploadIconBtn() {
    this.fileUpload.nativeElement.click();
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = (e: any) => {
      const image = new Image();
      image.src = e.target.result;
      image.onload = () => {
        this.ngZone.run(() => {
          this.imageWidth = image.width;
          this.imageHeight = image.height;
          console.log('Image width:', this.imageWidth);
          console.log('Image height:', this.imageHeight);
          if (this.imageWidth > 300) {
            this.imageTooLarge = true;
            this.selectedImageSrc = '';
            window.alert('圖片寬度超過300,請重新上傳');
            return;
          }
          if (this.imageHeight > 300) {
            this.imageTooLarge = true;
            this.selectedImageSrc = '';
            window.alert('圖片高度超過300,請重新上傳');
            return;
          }
          this.imageTooLarge = false;
          this.selectedImageSrc = e.target.result;
          const base64Image = this.getBase64Image(image);
          this.addItemCategoryVO.icon = base64Image;
        });
      };
    };
    reader.readAsDataURL(file);
  }

  getBase64Image(img: HTMLImageElement): string {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = img.width;
    canvas.height = img.height;
    ctx!.drawImage(img, 0, 0);
    const dataURL = canvas.toDataURL('image/jpeg');
    return dataURL;
  }



  submit() {
    if(this.utilsService.checkisNUll(this.addItemCategoryVO.icon) || this.utilsService.checkisNUll(this.addItemCategoryVO.name)){
      this.customDialog('提醒','請確認是否填寫完整');
      return;
    }
   if( this.utilsService.checkRepeatedName(this.addItemCategoryVO.name,this.itemCategoryList,"name")){
      this.customDialog('提醒','重複新增');
      return;
   }
   this.itemService.addItemcategory(this.addItemCategoryVO).then(async res => {
    if (res != null) {
     this.fetchData();
     this.addItemCategoryVO.icon = '';
     this.addItemCategoryVO.name = '';
     this.selectedImageSrc = '';
    }
  }
  ).catch(
    (e) => {
      this.customDialog("資料取得失敗！", "請聯繫資訊人員");
    })
  }

  cancelBtn() {
    this.selectedImageSrc = '';
    this.cardvisible = !this.cardvisible;
  }

  deleteIconBtn(itemCategory: ItemCategoryVO) {
  if(itemCategory.is_lock == 1){
    this.customDialog('提醒','該分類底下有物品，不可刪除');
      return;
    }

if (itemCategory.is_lock != 1) {
//跳確認視窗
this.confirmDialog('提醒','確認要刪除'+ itemCategory.name + '?').afterClosed()
.subscribe(async result => {
  if (result === true) {
   //點擊確認進入刪除流程
   var result =  await this.itemService.deleteItemCategory(itemCategory.id);
  if(result == null){
    //物品分類底下有物品無法刪除
    this.customDialog('提醒','刪除該物品分類失敗,請聯繫資訊人員');
    return;
  }
  this.customDialog('提醒','物品刪除成功');
   await this.fetchData();
   this.itemCategoryList = await this.itemService.getAllItemcategory();
   this.dataSource = new MatTableDataSource(this.itemCategoryList);
 }
 //點擊取消或關閉dialog
 return;
});
}

  }



  confirmDialog(title: string, content: string){
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(CommondialogComponent, { data: data });
    return dialogRef;
  }




  customDialog(title: string, content: string) {
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
    return dialogRef;
  }
}
