import { Component, OnInit } from '@angular/core';
import { ItemCategoryVO } from 'src/app/model/itemCategoryVO';
import { ItemService } from 'src/app/service/item.service';
import { AddItemVO } from 'src/app/model/addItemVO';
import { ItemVO } from 'src/app/model/itemVO';
import { MatTableDataSource } from '@angular/material/table';
import { UpdateItemVO } from 'src/app/model/updateItemVO';
import { ItemDistributeVO } from 'src/app/model/itemDistributeVO';
import { NumberItemVO } from 'src/app/model/NumberItemVO';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../shared/dialog/confirm-dialog/confirm-dialog.component';
import { UserGroupInfoVO } from 'src/app/model/userGroupInfoVO';
import { AccountService } from 'src/app/service/account.service';
import { GroupVO } from 'src/app/model/groupVO';
import { CommondialogComponent } from '../../shared/dialog/commondialog/commondialog.component';




@Component({
  selector: 'app-item-manage',
  templateUrl: './item-manage.component.html',
  styleUrls: ['./item-manage.component.scss']
})
export class ItemManageComponent implements OnInit {
  userAccount:string ='';
  userGroupInfo!: UserGroupInfoVO;
  isUserNoGroup:boolean = false;
  groupList!: GroupVO[];
  userAdminGroup!:GroupVO;
  itemCategoryList!: ItemCategoryVO[];
  selectedSearchItemCategory: ItemCategoryVO | null;
  itemList!: ItemVO[];
  cardvisible: boolean = false;
  updateCardvisible:boolean = false;
  updateItemid!:number;
  itemInfo!: ItemDistributeVO; //不需要編號的物品
  numberItemInfo!: NumberItemVO; //需要編號的物品
  unDistributeCount!:number;
  distributeCount!:number;
  filterText:string = '';
  addItemVO: AddItemVO = {
    category_id: 0,
    count: 0,
    is_lock: 0,
    is_map_point: 0,
    is_map_search: 0,
    is_number: 0,
    name: '',
    admin_group_id: 0
  };

  updateItem: UpdateItemVO = {
    category_id: 0,
    count: 0,
    is_lock: 0,
    is_map_point: 0,
    is_map_search: 0,
    is_number: 0,
    name: ''
  }
  displayedColumns: string[] = ['category_name', 'name', 'count', 'action'];
  dataSource = new MatTableDataSource(this.itemList);






  constructor(private itemService: ItemService, public dialog: MatDialog,public accountService:AccountService) {
    this.selectedSearchItemCategory = null;
   }




  ngOnInit(): void {
    this.fetchData();
  }

  exchange(element:any){
    const json = JSON.stringify(element);
    return encodeURIComponent(json);
  }


  async fetchData() {
    try {

      this.userAccount = localStorage.getItem('userAccount') ?? "";
      this.userGroupInfo = await this.accountService.getAccountAllGroupList(this.userAccount);
      if(this.userGroupInfo == null){
        this.isUserNoGroup = true;
      }
      this.groupList = this.userGroupInfo.group_info;
      this.groupList = this.groupList.filter(item => item.is_admin_group === 1);
      this.userAdminGroup = this.groupList[0]; //管理員只會存在一個管理員群組
      this.itemCategoryList = await this.itemService.getAllItemcategory();
      this.itemList = await this.itemService.getAdminItemList(this.userAdminGroup.id);
      this.dataSource = new MatTableDataSource(this.itemList);

    } catch (error) {
      console.error(error);
    }
  }


  addCardBtn() {
    this.cardvisible = !this.cardvisible;
    this.updateCardvisible = false;
  }


  mappingCategoryName(element:ItemVO){
  var object = this.itemCategoryList.find((item)=>item.id ==element.category_id) as ItemCategoryVO;
  return object.name;
  }


  async deleteItem(item:ItemVO) {
    await this.formatItemInfo(item);
    if(this.distributeCount >0){
      this.customDialog('提醒','物品目前已分配至單位,請先從群組刪除物品')
      return;
    }
    //進入刪除流程
    this.commonDialog('提醒','確定要刪除此物品嗎'+item.name+'?')
  .afterClosed()
  .subscribe(async result => {
    if (result === true) {
     //點擊確認進入刪除流程
     var result = await this.itemService.deleteItem(item.id);
     console.log(result)
       await this.fetchData()
       this.cardvisible = false;
       this.updateCardvisible = false;
    if(!result){
      //物品已經被打點無法刪除
      this.customDialog('提醒','刪除物品失敗請聯繫管理員');
      return;
    }
    this.customDialog('提醒','物品刪除成功');
     await this.fetchData();
     this.cardvisible = false;
     this.updateCardvisible = false;
   }
   //點擊取消或關閉dialog
   this.cardvisible = false;
   this.updateCardvisible = false;
   return;
  });



  }


  async editItem(element:ItemVO){
    await this.formatItemInfo(element);
    this.updateItemid = element.id;
     this.updateItem.category_id = element.category_id;
     this.updateItem.count = element.count;
     this.updateItem.is_lock = element.is_lock;
     this.updateItem.is_map_point = element.is_map_point;
     this.updateItem.is_map_search = element.is_map_search;
     this.updateItem.is_number = element.is_number;
     this.updateItem.name = element.name;
    this.updateCardvisible = !this.updateCardvisible;
    this.cardvisible = false;
  }


 async formatItemInfo(item:ItemVO){
    if(item.is_number == 0){
      this.itemInfo = await this.itemService.getItemInfo(item.id);
      this.unDistributeCount = this.itemInfo.unassigned_count;
      this.distributeCount = this.itemInfo.distribute_count;
    }
    //需要編號的物品
    if(item.is_number == 1){
      this.numberItemInfo = await this.itemService.getNumberItemInfo(item.id);
      this.unDistributeCount = this.numberItemInfo.unassigned_count;
      this.distributeCount = this.numberItemInfo.distribute_count;
    }
  }

resetAddItemVO(){
  this.addItemVO.category_id = 0;
  this.addItemVO.count = 0;
  this.addItemVO.is_lock = 0;
  this.addItemVO.is_map_point = 0;
  this.addItemVO.is_map_search = 0;
  this.addItemVO.is_number = 0;
  this.addItemVO.name = '';

}


 async submit() {
  console.log(this.addItemVO)
  this.addItemVO.admin_group_id = this.userAdminGroup.id;
    if(this.addItemVO.count ==0){
      this.customDialog('提醒','數量不可為0');
      return;
    }
    await this.itemService.addItem(this.addItemVO);
    this.fetchData();
    this.resetAddItemVO();
    this.cardvisible = false;
    this.updateCardvisible = false;
  }

  cancel(){
    this.cardvisible = false;
    this.updateCardvisible = false;
    this.resetAddItemVO();
  }

  updateCancel(){
    this.cardvisible = false;
    this.updateCardvisible = false;
  }


  async updateItemBtn(){

    var result = await this.checkUpdateCount(this.updateItem.count);
    if(!result){
      this.customDialog('提醒','數量不可小於已分配數量');
      return;
  }

    await this.itemService.updateItem(this.updateItem,this.updateItemid);
    await this.fetchData();
    this.cardvisible = false;
    this.updateCardvisible = false;
  }

  async checkUpdateCount(updateCount:number):Promise<boolean>{
      if(updateCount < this.distributeCount){
      return false;
      }
      return true;
  }


  clearSearchBtn(){
    this.filterText = '';
    this.selectedSearchItemCategory = null;
    this.fetchData();
  }

  findItem(event: Event) {
    if(this.selectedSearchItemCategory != undefined){
      return;
    }
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  findItemByCategory(selectedItemCategory: ItemCategoryVO | null){
    if(this.filterText != ''  ){
      return;
    }
    const filterValue = selectedItemCategory!.name.toLowerCase();
      this.dataSource.filter = filterValue;
      this.itemList = this.dataSource.filteredData;
  }


  customDialog(title: string, content: string) {
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
    return dialogRef;
  }

commonDialog(title: string, content: string) {
  let data = {
    "title": title,
    "content": content
  }
  let dialogRef = this.dialog.open(CommondialogComponent, { data: data });
  return dialogRef;
}



}
