
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BuildingVO } from 'src/app/model/buildingVO';
import { FloorVO } from 'src/app/model/floorVO';
import { ItemCategoryVO } from 'src/app/model/itemCategoryVO';
import { ItemVO } from 'src/app/model/itemVO';
import { BuildingService } from 'src/app/service/building.service';
import { ItemService } from 'src/app/service/item.service';
import { MapService } from 'src/app/service/map.service';
import { PointedItemVO } from 'src/app/model/PointedItemVO';
import { GroupItemInfoVO } from 'src/app/model/GroupItemInfoVO';






@Component({
  selector: 'app-item-find',
  templateUrl: './item-find.component.html',
  styleUrls: ['./item-find.component.scss']
})
export class ItemFindComponent implements OnInit {
  @ViewChild('canvas')
  canvas!: ElementRef<HTMLCanvasElement>;
  ctx!: CanvasRenderingContext2D;
  findBuilding!: BuildingVO;
  findFloor!: FloorVO
  building: string = ''
  floor: string = '';
  itemList!: ItemVO[];
  itemCategoryList!: ItemCategoryVO[];
  buildingList!: BuildingVO[];
  buildingFloorList!: FloorVO[]
  floorPointItemList!: PointedItemVO[]  //用floorID查
  groupItemInfoVO!:GroupItemInfoVO;







  constructor(private itemService: ItemService, private route: ActivatedRoute, private mapService: MapService, private buildingService: BuildingService) {

    this.route.queryParams.subscribe(params => {
      this.building = params['building'];
      this.floor = params['floor']

    });
  }

  ngOnInit() {
    this.fetchData()


  }



  async fetchData() {
    this.buildingList = await this.buildingService.getAllBuilding();
    this.buildingList = this.buildingList.filter((i) => i.build_name === this.building);
    this.findBuilding = this.buildingList[0];
    this.buildingFloorList = await this.buildingService.getBuildingFloorList(this.findBuilding.id);
    this.buildingFloorList = this.buildingFloorList.filter((floor) => floor.floor_name === this.floor);
    this.findFloor = this.buildingFloorList[0];
    console.log('呈現的大樓:', this.findBuilding)
    console.log('呈現的樓層:', this.findFloor)
    this.itemList = await this.itemService.getAllItem();
    this.itemCategoryList = await this.itemService.getAllItemcategory();
    // console.log(this.itemList)
    // console.log(this.itemCategoryList)
    this.floorPointItemList = await this.itemService.getFloorPointedItem(this.findFloor.id) //用樓層id找到該樓層的打點物品資訊
    // console.log('該樓層的打點物品資訊',this.floorPointItemList)
    await this.drawPointedItem();

  }






  async drawPointedItem() {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.ctx = canvasEl.getContext('2d') as CanvasRenderingContext2D;
    const context = canvasEl.getContext('2d');
    const img = new Image();
//pdf檔的情況
    if (this.findFloor.floor_Image_url.toLowerCase().endsWith('.pdf')) {
      console.log('是PDF文件');

  return;
    }
//jpg檔的情況
    img.src = this.findFloor.floor_Image_url;
    if (img.src != '') {
      img.onload = () => {
        canvasEl.width = img.width /8;
        canvasEl.height = img.height/5;
        context!.drawImage(img, 0, 0, canvasEl.width, canvasEl.height);
        //繪製已經打點的物品
        for (const point of this.floorPointItemList) {
          // console.log(point)

          this.drawItemInfo(Number(point.point_x), Number(point.point_y), point);
        }
      };

    }
    const infoElement = document.createElement('div');
    infoElement.style.position = 'fixed';
    infoElement.style.color = 'gray';
    infoElement.style.fontSize = '25px';
    infoElement.style.fontWeight = 'bold';
    infoElement.style.backgroundColor = 'white';
    document.body.appendChild(infoElement);
    canvasEl.addEventListener('mousemove', async (event) => {
      const rect = canvasEl.getBoundingClientRect(); // 取得 canvas 元素相對於視窗的位置
      const scaleX = canvasEl.width / rect.width; // 計算 x 軸的縮放比例
      const scaleY = canvasEl.height / rect.height; // 計算 y 軸的縮放比例
      const mouseX = (event.clientX - rect.left) * scaleX; // 轉換滑鼠 x 座標
      const mouseY = (event.clientY - rect.top) * scaleY;
      infoElement.style.top = `${event.clientY}px`;
      infoElement.style.left = `${event.clientX + 30}px`;
      let infoText = '';
      for (const point of this.floorPointItemList) {
        // 計算滑鼠位置與打點位置之間的距離
        const distance = Math.sqrt((mouseX - Number(point.point_x)) ** 2 + (mouseY - Number(point.point_y)) ** 2);
        if (distance < 30) {
          //沒有編號的物品
          if (point.item_number_id == '' && point.item_number_id_list == null) {
            const item = await this.findItemInfo(point)
            infoText += `${item}`;

          }
          //有編號的物品
          if (point.item_number_id !== '') {
            const item = await this.findItemInfo(point)
            infoText += `${item}<br>`;
          }
        }
        if (infoText.length > 0) {
          infoElement.innerHTML = infoText;
        } else {
          infoElement.textContent = '';
        }
      }
    })
  }





  async drawItemInfo(x: string | number, y: string | number, item: any) {
    const iconImage = new Image();
    const icon = await this.findItemIcon(item);
    if (icon) {
      iconImage.src = icon ?? ""
      iconImage.onload = () => {
        this.ctx.beginPath();
        this.ctx.arc(Number(x), Number(y), 10, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.drawImage(iconImage, Number(x) - 10, Number(y) - 10, 30, 30);
      }
    }

  }



  async findItemInfo(item: any) {
    var text = ''
    //沒有編號的物品
    if (item.item_number_id === '' && item.item_number_id_list == null) {
      const mappingObject = this.itemList.find((i) => i.id === item.item_id) as ItemVO;
      text = `${mappingObject.name} * ${item.count} `
      return text
    }
    //有編號的物品同一個位置也只有一個的情況
    if (item.item_number_id !== '') {
      var itemName = await this.findUuidItem(item);
      text = `${itemName}(${item.item_number_id}) `
      return text
    }


    return text
  }



 async findUuidItem(item:any){
   this.groupItemInfoVO = await this.mapService.findGroupItem(item.grouplist_id);
   const findObject = this.groupItemInfoVO.item_number_list.find((i)=>i.item_number_id === item.item_number_id);
      return findObject?.item_name
  }



  async findItemIcon(item: any) {

   //沒有編號的物品
   if(item.item_number_id === ''){
  //  console.log(item)
    const mappingObject = this.itemList.find((i) => i.id === item.item_id);
    const categoryObject = this.itemCategoryList.find((object) => object.id === mappingObject?.category_id)
    return categoryObject?.icon
   }
    //有編號的物品
    this.groupItemInfoVO = await this.mapService.findGroupItem(item.grouplist_id);
    const findObject = this.groupItemInfoVO.item_number_list.find((i)=>i.item_number_id === item.item_number_id);
    return findObject?.icon



  }


















}
