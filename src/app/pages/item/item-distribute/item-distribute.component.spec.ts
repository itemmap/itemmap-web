import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDistributeComponent } from './item-distribute.component';

describe('ItemDistributeComponent', () => {
  let component: ItemDistributeComponent;
  let fixture: ComponentFixture<ItemDistributeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDistributeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemDistributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
