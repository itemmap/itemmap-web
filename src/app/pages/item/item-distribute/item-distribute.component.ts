import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemVO } from 'src/app/model/itemVO';
import { NumberItemVO } from 'src/app/model/NumberItemVO';
import { ItemService } from 'src/app/service/item.service';
import { ItemDistributeVO } from 'src/app/model/itemDistributeVO';
import { MatTableDataSource } from '@angular/material/table';
import { GroupDistributeItemVO } from 'src/app/model/groupDistributeItemVO';
import { GroupService } from 'src/app/service/group.service';
import { UserGroupVO } from 'src/app/model/userGroupVO';
import { GroupAddItemVO } from 'src/app/model/groupAddItemVO';
import { GroupDistributeNumberItemVO } from 'src/app/model/GroupDistributeNumberItemVO';
import { ConfirmDialogComponent } from '../../shared/dialog/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { GroupVO } from 'src/app/model/groupVO';
import { CommondialogComponent } from '../../shared/dialog/commondialog/commondialog.component';
import { AccountService } from 'src/app/service/account.service';
import { UserGroupInfoVO } from 'src/app/model/userGroupInfoVO';




@Component({
  selector: 'app-item-distribute',
  templateUrl: './item-distribute.component.html',
  styleUrls: ['./item-distribute.component.scss']
})
export class ItemDistributeComponent implements OnInit {
  itemVO!: ItemVO;
  itemInfo!: ItemDistributeVO;
  numberItemInfo!: NumberItemVO;
  numberItemDistributeGroupList!: GroupDistributeNumberItemVO[]; //需要編號的物品
  itemDistributeGroupList!: GroupDistributeItemVO[];//不需要編號的物品
  groupList!: UserGroupVO[];
  displayedColumns: string[] = ['groupName', 'count'];
  groupDisplayedColumns: string[] = ['groupName', 'action'];
  itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList); //不需要編號的物品
  numberItemDistributeSource = new MatTableDataSource(this.numberItemDistributeGroupList); //需要編號的物品
  groupDataSource = new MatTableDataSource(this.groupList);
  addCountDisplay: boolean = false;
  selectedRow: any;
  isNeedNumberItem: boolean = false;
  checkResult:boolean = false
  groupAddItemVO: GroupAddItemVO = {
    item_id: 0,
    grouplist_id: 0,
    count: 0
  };
  userAccount:string = '';
  userGroupInfo!: UserGroupInfoVO;
  isUserNoGroup:boolean = false;
  userGroupList!: GroupVO[];
  userAdminGroup!:GroupVO;
  allGroupList!:GroupVO[];




  private dialogResultSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public dialogResult$ = this.dialogResultSubject.asObservable();


constructor(private accountService:AccountService,private route: ActivatedRoute, private itemService: ItemService, private groupService: GroupService,public dialog: MatDialog) { }



  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const itemStr = params.get('item');
     //判斷上一頁傳過來的物品是否有編號

      if (itemStr) {
        const decodedString = decodeURIComponent(itemStr);
        this.itemVO = JSON.parse(decodedString);
        if (this.itemVO.is_number == 1) {
          this.isNeedNumberItem = true;
        }
      }
    });
    this.fetchData();


  }

  async fetchData() {
    try {

      this.userAccount = localStorage.getItem('userAccount') ?? "";
      this.userGroupInfo = await this.accountService.getAccountAllGroupList(this.userAccount);
      if(this.userGroupInfo == null){
        this.isUserNoGroup = true;
      }

      this.userGroupList = this.userGroupInfo.group_info;
      this.userGroupList = this.userGroupList.filter(item => item.is_admin_group === 1);
      this.userAdminGroup = this.userGroupList[0];
      console.log(this.userAdminGroup)
      console.log(this.userGroupList)

      this.allGroupList = await this.groupService.getAllGroupList();
      console.log(this.allGroupList)

      this.groupList = await this.groupService.getGroupList();
      const findObject = this.groupList.filter(group =>group.group_name == this.userAdminGroup.name);
      console.log(findObject[0]);

      // this.groupList = this.groupList.filter(group =>group.group_name !== findObject[0].group_name)
      //不需要物品編號的物品
      if (!this.isNeedNumberItem) {
        this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
        this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
        // console.log(this.itemDistributeGroupList)
        this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
        this.checkGroupList();
      }

      //需要物品編號的物品
      if (this.isNeedNumberItem) {
        this.numberItemInfo = await this.itemService.getNumberItemInfo(this.itemVO.id);
        this.numberItemDistributeGroupList = this.numberItemInfo.item_number_list;
        // console.log(this.numberItemDistributeGroupList)
        this.numberItemDistributeSource = new MatTableDataSource(this.numberItemDistributeGroupList);
      }
      this.groupDataSource = new MatTableDataSource(this.groupList);
    } catch (error) {
      console.error(error);
    }
  }


  checkGroupList() {
    for (let i = this.groupList.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.itemDistributeGroupList.length; j++) {
        if (this.groupList[i].group_id === this.itemDistributeGroupList[j].grouplist_id) {
          this.groupList.splice(i, 1);
          break;
        }
      }
    }
  }


async  deleteNumberItem(element: any) {
  this.confirmDialog('提醒','確定要刪除此物品嗎')
  .afterClosed()
  .subscribe(async result => {
    if (result === true) {
     //點擊確認進入刪除流程
     var result = await this.itemService.deleteNumberItem(element.item_number_id);
    if(result == null){
      //物品已經被打點無法刪除
      this.customDialog('提醒','該物品已經被打點無法刪除');
      return;
    }
    this.customDialog('提醒','物品刪除成功');
     await this.fetchData();
   }
   //點擊取消或關閉dialog
   return;
  });

  }

 async deleteItem(element:any){
  this.confirmDialog('提醒','確定要刪除此物品嗎')
  .afterClosed()
  .subscribe(async result => {
    if (result === true) {
     //點擊確認進入刪除流程
     var result = await this.itemService.deleteDistributeItem(element.id);
    if(result == null){
      //物品已經被打點無法刪除
      this.customDialog('提醒','該物品已經被打點無法刪除');
      return;
    }
    this.customDialog('提醒','物品刪除成功');
     await this.fetchData();
   }
   //點擊取消或關閉dialog
   return;
  });

  }

  addBtn(element: any) {
    this.selectedRow = element;
    this.addCountDisplay = !this.addCountDisplay;
  }

 async updateItemCount(element:any){
   if(element.count == 0){
    this.customDialog('提醒','數量不可為0');
    this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
    this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
    this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
    return;
   }

    var result =  await this.itemService.updateItemCount(element.id,element.count);
    if(!result ){
      this.customDialog('提醒','更新數量失敗,請聯絡資訊人員');
      return;
    }
    if(result){
        this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
        this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
        this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
        this.customDialog('提醒','更新數量成功');
    }
  }



 async submit(element: any) {
    if(this.itemInfo != null && this.groupAddItemVO.count > this.itemInfo.unassigned_count){
      this.customDialog('提醒','超過該物品未分配數量');
      return;
    }
    if(this.numberItemInfo != null && this.groupAddItemVO.count > this.numberItemInfo.unassigned_count){
      this.customDialog('提醒','超過該物品未分配數量');
      return;
    }
    if(this.groupAddItemVO.count == 0){
      this.customDialog('提醒','分配數量不可為0');
      return;
    }
    this.groupAddItemVO.item_id = this.itemVO.id;
    this.groupAddItemVO.grouplist_id = element.group_id;
     await this.itemService.distributeToGroup(this.groupAddItemVO, this.isNeedNumberItem);
    this.groupList = await this.groupService.getGroupList();
    this.groupDataSource = new MatTableDataSource(this.groupList);

    if (!this.isNeedNumberItem) {
      this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
      this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
      this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
    }

    //需要物品編號的物品
    if (this.isNeedNumberItem) {
      this.numberItemInfo = await this.itemService.getNumberItemInfo(this.itemVO.id);
      this.numberItemDistributeGroupList = this.numberItemInfo.item_number_list;
      this.numberItemDistributeSource = new MatTableDataSource(this.numberItemDistributeGroupList);
    }





  }





  customDialog(title: string, content: string) {
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
    dialogRef.afterClosed().subscribe(async result => {
    await  this.dialogResultSubject.next(result);
    });
  }


  confirmDialog(title: string, content: string){
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(CommondialogComponent, { data: data });
    return dialogRef;
  }


}
