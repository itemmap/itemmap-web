import { UtilsService } from 'src/app/util/utils.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs/internal/Observable';
import { UserProfileVO } from 'src/app/model/userProfileVO';
import { BasicService } from 'src/app/service/basic.service';
import { FindUserInfoVO } from 'src/app/model/findUserInfoVO';
import { MemberVO } from 'src/app/model/memberVO';
import { AccountService } from 'src/app/service/account.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../shared/dialog/confirm-dialog/confirm-dialog.component';
import { DeptAccountVO } from 'src/app/model/deptAccountVO';



export interface PeriodicElement {
  name: string;
  account: number;
}
@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
})
export class UserAddComponent implements OnInit {
  displayedColumnsGroup: string[] = ['account', 'name'];
  accountList: MemberVO[] = [];
  memberSource = new MatTableDataSource(this.accountList);
  hisAccount = '';
  userProfile!: UserProfileVO;
  findUserInfoVO: FindUserInfoVO = {
    name: '',
    id_number: '',
    user_department: '',
  };
  filtertext = '';
  selectedRowIndex: number = -1;
  memberTotalCount!:number;
  addUserResult:boolean = false;


  constructor(
    private basicService: BasicService,
    private accountService: AccountService,
    private utilsService:UtilsService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.fetchData();
  }

  async fetchData() {

      this.accountList = await this.accountService.getAllAccount();
      this.memberSource = new MatTableDataSource(this.accountList);
      this.memberTotalCount = this.accountList.length;

  }

 async findAccountAtHis() {
    this.userProfile = await this.basicService.getUserProfile(this.hisAccount);
      if (this.userProfile !== null) {
        this.findUserInfoVO.name = this.userProfile.emp_name;
        this.findUserInfoVO.id_number = this.hisAccount;
        this.findUserInfoVO.user_department = this.userProfile.emp_dept;
      }
     if(this.userProfile.emp_name == '' ) {
      this.customDialog('提醒','查無此用戶');
      }
  }

  async addAccountBtn(findUserInfoVO: FindUserInfoVO) {
    if(this.utilsService.checkRepeatedName(findUserInfoVO.id_number,this.accountList,"identity_number")){
      this.customDialog('提醒','重複新增');
      return;
    }

  //先把使用者加入ldap
   this.addUserResult = await this.accountService.addUserToLDAP(this.findUserInfoVO.id_number,this.findUserInfoVO.name,this.findUserInfoVO.user_department);
   if(!this.addUserResult){
    this.customDialog('提醒','新增用戶失敗請聯繫系統管理人員');
    return;
   }

    // 如果api回傳是true才寫進系統DB
    if(this.addUserResult){
    await this.accountService.addAccount(findUserInfoVO);
    await this.fetchData();

    }

  }

  findAccount(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.memberSource.filter = filterValue.trim().toLowerCase();
    const filteredData = this.memberSource.filteredData;
    this.memberTotalCount = filteredData.length;
  }



  customDialog(title: string, content: string) {
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
    return dialogRef;
  }



}
