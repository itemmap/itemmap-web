import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserLocalstorageService } from 'src/app/util/user-localstorage.service';
import { ResponseModel } from '../../model/responseModel';
import { environment } from 'src/environments/environment';
import { ResponseDef } from '../../constant/ResponseDef';
import { Router } from '@angular/router';
import { LoginRequestVO } from 'src/app/model/login-requestVO';
import { UserProfileVO } from 'src/app/model/userProfileVO';
import { UserToken } from 'src/app/model/userTokenVO';
import jwt_decode from "jwt-decode";
import { UserHrInfoVO } from 'src/app/model/userHrInfoVO';
import { ConfirmDialogComponent } from '../shared/dialog/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(
    private http: HttpClient,
    private userLocalstorageService: UserLocalstorageService,
    private router: Router,
    public dialog: MatDialog
  ) { }
  user!: UserProfileVO;
  loginRequest: LoginRequestVO = {
    account: '',
    password: '',
    project: "itemmap"
  };


  errorVisible: boolean = false;
  errorMessage = '';
  customStylesValidated: boolean = false;

  ngOnInit(): void {
    this.userLocalstorageService.deleUserInfo();
  }

  login() {
    if(this.loginRequest.password == '' || this.loginRequest.account == ''){
      this.customDialog('提醒','帳號密碼不能為空')
      return;
    }
    const url = environment.apiEndpoint + '/auth/loginHis';
    this.customStylesValidated = true;
    this.http
      .post<ResponseModel>(url, this.loginRequest)
      .subscribe((responseData) => {
        this.userLocalstorageService.saveUserToken(responseData.result['token'])
        const Token: UserToken = jwt_decode(responseData.result['token']) ;
        const userProfile: UserHrInfoVO = JSON.parse(Token.userProfile)
        console.log(userProfile)
        if (userProfile.success == false) {
          this.customDialog('提醒','登入失敗')
          console.log('登入失敗');
          return;
        } else {
          this.userLocalstorageService.saveUserToken(responseData.result['token'])
          const Token: UserToken = jwt_decode(responseData.result['token']) ;
          const userProfile: UserHrInfoVO = JSON.parse(Token.userProfile)
          this.user = userProfile.data
          // console.log(this.user.emp_name)
          this.userLocalstorageService.saveUserRole(Token.role);
          this.userLocalstorageService.saveUserName(
            this.user.emp_name
          );
          this.userLocalstorageService.saveUserAccount(this.loginRequest.account);
          this.userLocalstorageService.saveUserDepartment(this.user.emp_dept);
          this.router.navigate(['/item/point']);
          console.log('登入成功');
        }
      });
  }


  customDialog(title: string, content: string) {
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
    return dialogRef;
  }






}
