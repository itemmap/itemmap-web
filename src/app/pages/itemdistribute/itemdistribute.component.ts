import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { GroupDistributeNumberItemVO } from 'src/app/model/GroupDistributeNumberItemVO';
import { NumberItemVO } from 'src/app/model/NumberItemVO';
import { GroupAddItemVO } from 'src/app/model/groupAddItemVO';
import { GroupDistributeItemVO } from 'src/app/model/groupDistributeItemVO';
import { GroupVO } from 'src/app/model/groupVO';
import { ItemDistributeVO } from 'src/app/model/itemDistributeVO';
import { ItemVO } from 'src/app/model/itemVO';
import { NeedNumberItemVO } from 'src/app/model/needNumberItemVO';
import { UserGroupInfoVO } from 'src/app/model/userGroupInfoVO';
import { AccountService } from 'src/app/service/account.service';
import { GroupService } from 'src/app/service/group.service';
import { ItemService } from 'src/app/service/item.service';
import { CommondialogComponent } from '../shared/dialog/commondialog/commondialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../shared/dialog/confirm-dialog/confirm-dialog.component';
import { group } from '@angular/animations';

@Component({
  selector: 'app-itemdistribute',
  templateUrl: './itemdistribute.component.html',
  styleUrls: ['./itemdistribute.component.scss']
})
export class ItemdistributeComponent implements OnInit{
  itemVO!: ItemVO;
  isNeedNumberItem:boolean = false;
  groupList!:GroupVO[];
  userGroupList!:GroupVO[];
  userAccount:string = '';
  userGroupInfo!: UserGroupInfoVO;
  isUserNoGroup:boolean = false;
  userAdminGroup!:GroupVO;
  groupDataSource = new MatTableDataSource(this.groupList);
  groupDisplayedColumns: string[] = ['groupName', 'action'];
  selectedRow: any;
  addCountDisplay: boolean = false;
  noNumberItemInfo!: ItemDistributeVO;
  itemInfo!: ItemDistributeVO;
  numberItemInfo!: NumberItemVO;
  numberItemDistributeGroupList!: GroupDistributeNumberItemVO[]; //需要編號的物品
  itemDistributeGroupList!: GroupDistributeItemVO[];//不需要編號的物品
  itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
  numberItemDistributeSource = new MatTableDataSource(this.numberItemDistributeGroupList);
  displayedColumns:string[] = ['groupName', 'count'];
  groupAddItemVO: GroupAddItemVO = {
    item_id: 0,
    grouplist_id: 0,
    count: 0
  };

  constructor(public dialog: MatDialog,private route: ActivatedRoute,public itemService:ItemService,private groupService:GroupService,private accountService:AccountService){}


  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const itemStr = params.get('item');
     //判斷上一頁傳過來的物品是否有編號
      if (itemStr) {
        const decodedString = decodeURIComponent(itemStr);
        this.itemVO = JSON.parse(decodedString);
        if (this.itemVO.is_number == 1) {
          this.isNeedNumberItem = true;

        }
      }
    });
    this.fetchData();

}


  async fetchData(){
    this.userAccount = localStorage.getItem('userAccount') ?? "";
    this.userGroupInfo = await this.accountService.getAccountAllGroupList(this.userAccount);
    if(this.userGroupInfo == null){
      this.isUserNoGroup = true;
    }
    this.userGroupList = this.userGroupInfo.group_info;
    this.userGroupList = this.userGroupList.filter(item => item.is_admin_group === 1);
    this.userAdminGroup = this.userGroupList[0];
    this.groupList = await this.groupService.getAllGroupList();
    this.groupList = this.groupList.filter(group=>group.is_admin_group !==1)
    this.groupList.push( this.userAdminGroup)


    await  this.settingData()


}

async settingData(){
  //不需要物品編號的物品
  if (!this.isNeedNumberItem) {
    this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
    this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
    this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
    this.checkGroupList();
    this.groupDataSource = new MatTableDataSource(this.groupList);
  }

 //需要物品編號的物品
 if (this.isNeedNumberItem) {
  this.numberItemInfo = await this.itemService.getNumberItemInfo(this.itemVO.id);
  this.numberItemDistributeGroupList = this.numberItemInfo.item_number_list;
  this.numberItemDistributeSource = new MatTableDataSource(this.numberItemDistributeGroupList);
  this.groupDataSource = new MatTableDataSource(this.groupList);
}
}


checkGroupList(){
  //把已經分配過的group從 this.groupList去除
  for (let i = this.groupList.length - 1; i >= 0; i--) {
    for (let j = 0; j < this.itemDistributeGroupList.length; j++) {
      if (this.groupList[i].id === this.itemDistributeGroupList[j].grouplist_id) {
        this.groupList.splice(i, 1);
        break;
      }
    }
  }

}


async updateItemCount(item:any){
  if(item.count == 0){
    this.customDialog('提醒','數量不可為0');
    this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
    this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
    this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
    return;
   }

    var result =  await this.itemService.updateItemCount(item.id,item.count);
    if(!result ){
      this.customDialog('提醒','更新數量失敗,請聯絡資訊人員');
      return;
    }
    if(result){
        this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
        this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
        this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
        this.customDialog('提醒','更新數量成功');
    }
}


deleteItem(item:any){

  this.confirmDialog('提醒','確定要刪除此物品嗎')
  .afterClosed()
  .subscribe(async result => {
    if (result === true) {
     //點擊確認進入刪除流程
     var result = await this.itemService.deleteDistributeItem(item.id);
    if(result == null){
      //物品已經被打點無法刪除
      this.customDialog('提醒','該物品已經被打點無法刪除');
      return;
    }
    this.customDialog('提醒','物品刪除成功');
     await this.fetchData();
   }
   //點擊取消或關閉dialog
   return;
  });
}


deleteNumberItem(item:any){

  this.confirmDialog('提醒','確定要刪除此物品嗎')
  .afterClosed()
  .subscribe(async result => {
    if (result === true) {
     //點擊確認進入刪除流程
     var result = await this.itemService.deleteNumberItem(item.item_number_id);
    if(result == null){
      //物品已經被打點無法刪除
      this.customDialog('提醒','該物品已經被打點無法刪除');
      return;
    }
    this.customDialog('提醒','物品刪除成功');
     await this.fetchData();
   }
   //點擊取消或關閉dialog
   return;
  });
}


async submit(element:any){
  if(this.itemInfo != null && this.groupAddItemVO.count > this.itemInfo.unassigned_count){
    this.customDialog('提醒','超過該物品未分配數量');
    return;
  }
  if(this.numberItemInfo != null && this.groupAddItemVO.count > this.numberItemInfo.unassigned_count){
    this.customDialog('提醒','超過該物品未分配數量');
    return;
  }
  if(this.groupAddItemVO.count == 0){
    this.customDialog('提醒','分配數量不可為0');
    return;
  }

  this.groupAddItemVO.item_id = this.itemVO.id;
  this.groupAddItemVO.grouplist_id = element.id;
  await this.itemService.distributeToGroup(this.groupAddItemVO, this.isNeedNumberItem);
  if (!this.isNeedNumberItem) {
    this.itemInfo = await this.itemService.getItemInfo(this.itemVO.id);
    this.itemDistributeGroupList = this.itemInfo.item_distribute_group_list;
    this.itemDistributeSource = new MatTableDataSource(this.itemDistributeGroupList);
  }

  //需要物品編號的物品
  if (this.isNeedNumberItem) {
    this.numberItemInfo = await this.itemService.getNumberItemInfo(this.itemVO.id);
    this.numberItemDistributeGroupList = this.numberItemInfo.item_number_list;
    this.numberItemDistributeSource = new MatTableDataSource(this.numberItemDistributeGroupList);
  }
  await  this.settingData();

  }






addBtn(element: any) {
  this.selectedRow = element;
  this.addCountDisplay = !this.addCountDisplay;
}



customDialog(title: string, content: string) {
  let data = {
    "title": title,
    "content": content
  }
  let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
  return dialogRef;
}

confirmDialog(title: string, content: string){
  let data = {
    "title": title,
    "content": content
  }
  let dialogRef = this.dialog.open(CommondialogComponent, { data: data });
  return dialogRef;
}


}
