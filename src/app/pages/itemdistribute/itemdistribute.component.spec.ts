import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemdistributeComponent } from './itemdistribute.component';

describe('ItemdistributeComponent', () => {
  let component: ItemdistributeComponent;
  let fixture: ComponentFixture<ItemdistributeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemdistributeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemdistributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
