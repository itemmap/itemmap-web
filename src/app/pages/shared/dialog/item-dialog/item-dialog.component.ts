import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { InputDialog } from 'src/app/model/inputDialog';
import { PointItemVO } from 'src/app/model/pointItemVO';


export interface PointItemInfoVO {
  title:string;
  content:PointItemVO


}




@Component({
  selector: 'app-item-dialog',
  templateUrl: './item-dialog.component.html',
  styleUrls: ['./item-dialog.component.scss']
})



export class ItemDialogComponent {
  itemInfo!:PointItemVO;

  constructor(
    public dialogRef: MatDialogRef<ItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InputDialog,
    ) {
      // console.log(data.content)
      this.itemInfo = data.content as unknown as PointItemVO;
    }


  onNoClick(): void {
    this.dialogRef.close();
  }
}
