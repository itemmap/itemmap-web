import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { InputDialog } from 'src/app/model/inputDialog';

@Component({
  selector: 'app-commondialog',
  templateUrl: './commondialog.component.html',
  styleUrls: ['./commondialog.component.scss']
})
export class CommondialogComponent {

  constructor(
    public dialogRef: MatDialogRef<CommondialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InputDialog,
    ) {}


  onNoClick(): void {
    this.dialogRef.close();
  }
}
