import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-loading-com',
  templateUrl: './loading-com.component.html',
  styleUrls: ['./loading-com.component.scss']
})
export class LoadingComComponent implements OnInit {

  @Input()
  loadingVisibleClass!: string;
  constructor() { }

  ngOnInit(): void {
  }
}
