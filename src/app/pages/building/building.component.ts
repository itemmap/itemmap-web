import { Component } from '@angular/core';
import { BuildingService } from 'src/app/service/building.service';
import { BuildingVO } from 'src/app/model/buildingVO';
import { FloorVO } from 'src/app/model/floorVO';
import { GroupVO } from 'src/app/model/groupVO';
import { MatTableDataSource } from '@angular/material/table';
import { LightBoxPictureVO } from 'src/app/model/lightBoxPictureVO';

@Component({
  selector: 'app-building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.scss']
})
export class BuildingComponent {
  buildingList!: BuildingVO[];
  floorList!: FloorVO[];
  selectedBuilding!: BuildingVO;
  selectedFloor!: FloorVO;
  groupList!: GroupVO[];
  displayedColumnsGroup: string[] = ['group'];
  groupSource = new MatTableDataSource(this.groupList);
  selectedRowIndex: number = -1;
  previewImage = false;
  showMask = false;
  currentLightBoxImage: LightBoxPictureVO = {
    imageSrc: '',
    imageAlt: ''
  };


  constructor(private buildingService: BuildingService) { }
  ngOnInit(): void {
    this.fetchData();

  }

  async fetchData() {
    try {
      this.buildingList = await this.buildingService.getAllBuilding();
      this.selectedBuilding = this.buildingList[0];
      this.findBuildingFloors(this.selectedBuilding);
    } catch (error) {
      console.error(error);
    }
  }

  async findBuildingFloors(building: BuildingVO) {
    try {
      this.floorList = await this.buildingService.getBuildingFloorList(building.id);
      this.selectedFloor = this.floorList[0];
      this.findFloorGroups(this.selectedFloor)
    } catch (error) {
      console.error(error);
    }
  }

  async findFloorGroups(floor: FloorVO) {
    try {
      this.selectedFloor = floor;
      this.groupList = await this.buildingService.getFloorGroupList(floor.id);
      this.groupSource = new MatTableDataSource(this.groupList);
    } catch (error) {
      console.error(error);
    }
  }



  onPreviewImage() {
    this.currentLightBoxImage.imageAlt = this.selectedFloor.floor_name;
    this.currentLightBoxImage.imageSrc = this.selectedFloor.floor_Image_url;
    this.showMask = true;
    this.previewImage = true;
  }


  closePicture() {
    this.showMask = false;
    this.previewImage = false;
  }

}
