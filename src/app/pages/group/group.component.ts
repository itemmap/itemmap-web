import { Component, OnInit } from '@angular/core';
import { AccountVO, UserGroupVO } from 'src/app/model/userGroupVO';
import { GroupService } from 'src/app/service/group.service';
import { AccountService } from 'src/app/service/account.service';
import { MatTableDataSource } from '@angular/material/table';
import { MemberVO } from 'src/app/model/memberVO';
import { GroupAddMemberRequestVO } from 'src/app/model/group-addMember-requestVO';
import { RemoveMemberRequestVO } from 'src/app/model/removeMember-requestVO';
import { GroupVO } from 'src/app/model/groupVO';
import { ConfirmDialogComponent } from '../shared/dialog/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
})
export class GroupComponent implements OnInit {
  allGroupList!: GroupVO[];
  groupList!: UserGroupVO[];
  accountList!: MemberVO[];
  groupUserList!: AccountVO[];
  groupMemberSource = new MatTableDataSource(this.groupUserList);
  memberSource = new MatTableDataSource(this.accountList);
  displayedColumnsGroup: string[] = ['account', 'name', 'actions'];
  selected: any;
  filtertext = '';
  selectedRowIndex: number = -1;
  selectedRow: number = -1;
  memberTotalCount!: number;
  groupMemberTotalCount!: number;
  addMemberRequestVO: GroupAddMemberRequestVO = {
    user_id: 0,
    group_list_id: 0
  };
  removeMemberRequestVO: RemoveMemberRequestVO = {
    groupid: 0,
    userid: 0
  }
  isAdminGroup: boolean = false;
  userDepartment: string = '';
  adminGroupList: string[] = ['測試-工務', '測試-總務', '測試-職安']
  selectedGroup!: GroupVO;
  foundObject: GroupVO = {
    id: 0,
    name: '',
    is_admin_group: 0
  };




  constructor(
    private groupService: GroupService,
    private accountService: AccountService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.fetchData();
  }

  async fetchData() {
    try {
      this.userDepartment = localStorage.getItem('userDepartment') ?? "";
      // this.allGroupList = this.testGroupList;
      this.allGroupList = await this.groupService.getAllGroupList();
      // this.foundObject = this.allGroupList.find(obj => obj.name.includes(this.userDepartment)) as GroupVO;
      // console.log(this.foundObject);
      // this.allGroupList = this.allGroupList.filter(group => group.is_admin_group !=1 );
      // this.allGroupList.push(this.foundObject)
      // console.log(this.allGroupList)

      this.accountList = await this.accountService.getAllAccount();
      this.memberSource = new MatTableDataSource(this.accountList);
      this.selectedGroup = this.allGroupList[0];

      this.findGroupMembers(this.selectedGroup)
      this.memberTotalCount = this.accountList.length;
    } catch (error) {
      console.error(error);
    }
  }




  async findGroupMembers(groupItem: GroupVO) {
    console.log(this.selectedGroup)
    this.groupUserList = await this.accountService.getGroupAccount(
      groupItem.id
    );
    if (this.groupUserList == null) {
      this.groupMemberTotalCount = 0;
      this.groupMemberSource = new MatTableDataSource(this.groupUserList);
      return;
    }
    this.groupMemberTotalCount = this.groupUserList.length;
    this.groupMemberSource = new MatTableDataSource(this.groupUserList);
  }

  findAccount(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.memberSource.filter = filterValue.trim().toLowerCase();
    const filteredData = this.memberSource.filteredData;
    this.memberTotalCount = filteredData.length;
  }



  async checkMemberIsInGroup(member: MemberVO) {
    // await this.findGroupMembers(this.selectedGroup)
    // console.log(this.groupUserList)
    //檢查選到的群組裡的使用者裡是否已經存在同樣身分證字號的用戶
    for (const groupUser of this.groupUserList) {
      if (groupUser.user_identity_number == member.identity_number) {
        return false
      }
    }
    return true

  }





  async addMemberToGroup(member: MemberVO) {

// console.log(this.groupUserList)
if(this.groupUserList !== null){
    var checkResult = await this.checkMemberIsInGroup(member);
    if (!checkResult) {
      this.customDialog('提醒', '此用戶已在此群組內');
      return;
    }
  }

    // console.log(member)
    this.addMemberRequestVO.user_id = member.id;
    this.addMemberRequestVO.group_list_id = this.selectedGroup.id;
    console.log(this.addMemberRequestVO)
    await this.groupService.addMember(this.addMemberRequestVO);
    this.groupUserList = await this.accountService.getGroupAccount(this.selectedGroup.id);
    console.log(this.groupUserList);
    // this.groupUserList = groupUserList;
    this.groupMemberSource = new MatTableDataSource(this.groupUserList);
    this.groupMemberTotalCount = this.groupUserList.length;

  }

  removeMemberFromGroup(member: AccountVO) {
    this.removeMemberRequestVO.groupid = member.group_id;
    this.removeMemberRequestVO.userid = member.user_id;
    this.groupService.removeMember(this.removeMemberRequestVO).then(() => {
      this.accountService.getGroupAccount(this.selectedGroup.id)
        .then((groupUserList: AccountVO[]) => {
          this.groupUserList = groupUserList;
          this.groupMemberSource = new MatTableDataSource(this.groupUserList);
          this.groupMemberTotalCount = this.groupUserList.length;
        });
    });
  }


  customDialog(title: string, content: string) {
    let data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
    return dialogRef;
  }
}
