import { Injectable } from '@angular/core';
import { UserLocalstorageService } from './user-localstorage.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/pages/shared/dialog/confirm-dialog/confirm-dialog.component';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private dialog: MatDialog,private userLocalstorageService:UserLocalstorageService, private router: Router) { }


  handleAuthError(err: any) {
    console.log(err)
    if (err.status === 401 || err.status === 403 || err.status === 0) {
      this.userLocalstorageService.deleUserInfo();
      this.customDialog("登入時間過期", "請在重新登入一次");
    }
  }

  customDialog(title: string, content: string) {
    var data = {
      "title": title,
      "content": content
    }
    let dialogRef = this.dialog.open(ConfirmDialogComponent, { data: data });
    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          this.router.navigate(['/login']);
        }
      }
    )
  }

  checkRepeatedName(itemName:string,itemList:any[], key:string){
    for (const item of itemList) {
      if (item[key] === itemName) {
        return true;
      }
    }
    return false;
  }

  checkisNUll(text: string){
    return text =="" || text == undefined;  //return true or false
  }


}
