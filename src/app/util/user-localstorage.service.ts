import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { UserProfileVO } from '../model/userProfileVO';



@Injectable({
  providedIn: 'root'
})
export class UserLocalstorageService {
  user!:UserProfileVO

  constructor() { }

  saveUserToken(userToken: string){
    localStorage.setItem('userToken', userToken);

  }

  saveUserName(userAccount:string){
    localStorage.setItem('userName', userAccount);

  }

  saveUserRole(roleList:string){
    localStorage.setItem('roleList',roleList);
  }

  saveUserDepartment(userDepartment:string){
    localStorage.setItem('userDepartment', userDepartment);
  }


  saveUserAccount(userAccount:string){
    localStorage.setItem('userAccount', userAccount);
  }



  deleUserInfo(){
      localStorage.clear();
  }
}
