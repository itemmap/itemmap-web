import { INavData } from '@coreui/angular';


//管理者權限
export const navItemsAdmin: INavData[] = [

  {
    title: true,
    name: '物品管理',

  },
  {
    name: '物品分類管理',
    url: '/item/category',
    iconComponent: { name: 'cilInbox' }
  },
  {
    name: '物品項目管理',
    url: '/item/manage',
    iconComponent: { name: 'cilSitemap' }
  },
  {
    name: '物品標記',
    url: '/item/point',
    iconComponent: { name: 'cil-golf' }
  },
  {
    name: '大樓管理',
    url: '/building/list',
    iconComponent: { name: 'cilBuilding' }
  },
  {
    title: true,
    name: '人員管理',

  },
  {
    name: '用戶管理',
    url: '/member/add',
    iconComponent: { name: 'cilUser' }
  },
  {
    name: '群組管理',
    url: '/group/list',
    iconComponent: { name: 'cil-people' }
  },
  {
    name: '登出',
    url: '/login',
    iconComponent: { name: 'cilAccountLogout' }
  }

];







export const userNavItems: INavData[] = [


  {
    name: '大樓管理',
    url: '/building/list',
    iconComponent: { name: 'cilBuilding' }
  },
  {
    name: '物品標記',
    url: '/item/point',
    iconComponent: { name: 'cil-golf' }
  },

  {
    name: '登出',
    url: '/login',
    iconComponent: { name: 'cilAccountLogout' }
  }

];



