import { Component ,OnInit } from '@angular/core';
import { UserProfileVO } from 'src/app/model/userProfileVO';
import { navItemsAdmin, userNavItems } from './_nav';
import { BasicService } from 'src/app/service/basic.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserLocalstorageService } from 'src/app/util/user-localstorage.service';
import { AccountService } from 'src/app/service/account.service';
import { UserGroupInfoVO } from 'src/app/model/userGroupInfoVO';
import { GroupVO } from 'src/app/model/groupVO';
import { ConnectionPositionPair } from '@angular/cdk/overlay';
import { GroupService } from 'src/app/service/group.service';
import { group } from '@angular/animations';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
})
export class DefaultLayoutComponent implements OnInit{
  userProfile!: Observable<UserProfileVO | null>;
  userRole:string = '';
  userName:string ='';
  userDepartment:string = '';
  navItems = navItemsAdmin;
  titleName:string = '物品管理系統';
  perfectScrollbarConfig = {
    suppressScrollX: true,
  };
  userAccount:string = '';
  userGroupInfo!: UserGroupInfoVO;
  isUserNoGroup:boolean =false;
  groupList!: GroupVO[];




  constructor(public groupService:GroupService,public accountService:AccountService,public basicService: BasicService,private router: Router , private userLocalstorageService: UserLocalstorageService, ) {}
  ngOnInit(): void {
    this.userName = localStorage.getItem('userName')!
    this.userDepartment = localStorage.getItem('userDepartment')!
    this.fetchUserGroupList();
    this.
    initUserRole(localStorage.getItem('roleList')!)


  }





 async fetchUserGroupList(){
    this.userAccount = localStorage.getItem('userAccount') ?? "";
    this.userGroupInfo = await this.accountService.getAccountAllGroupList(this.userAccount);
    if(this.userGroupInfo == null){
      this.isUserNoGroup = true;
    }
    this.groupList = this.userGroupInfo.group_info;
    // console.log(this.groupList)
  }


  initUserRole(role:string){
    if(role == "admin"){
      this.userRole = '管理員'
      return;
    }
    this.userRole  = '';
    this.navItems = userNavItems
  }

}
