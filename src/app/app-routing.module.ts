import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultLayoutComponent } from './containers';
import { AuthGuard } from './auth/auth.guard';
import { ItemCategoryComponent } from './pages/item/item-category/item-category.component';
import {  ItemPointComponent } from './pages/item/item-point/item-point.component';
import { UserAddComponent } from './pages/user/user-add/user-add.component';
import { LoginComponent } from './pages/login/login.component';
import { GroupComponent } from './pages/group/group.component';
import { BuildingComponent } from './pages/building/building.component';
import { ItemManageComponent } from './pages/item/item-manage/item-manage.component';
import { ItemdistributeComponent } from './pages/itemdistribute/itemdistribute.component';
import { ItemFindComponent } from './pages/item/item-find/item-find.component';






const routes: Routes = [

  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home'
    },
    children: [

      {
        path: 'item',
        children: [
          { path: 'category', component:  ItemCategoryComponent},
          { path: 'manage', component: ItemManageComponent },
          { path: 'point', component: ItemPointComponent },
          { path: 'distribute', component: ItemdistributeComponent },

        ]
      },
      {
        path: 'building',
        children: [
          { path: 'list', component:  BuildingComponent},

        ]
      },
      {
        path: 'member',
        children: [

          { path: 'add', component: UserAddComponent },
        ]
      },
      {
        path: 'group',
        children: [
          { path: 'list', component:  GroupComponent},

        ]
      },

    ]
  },



  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  }
,
  {
    path:'item/point/find',
    component: ItemFindComponent,
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
      anchorScrolling: 'enabled',
      initialNavigation: 'enabledBlocking'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
