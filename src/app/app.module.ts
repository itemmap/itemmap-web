import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatListModule} from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { GroupItemsByCategoryPipe } from './service/filter-by-category.pipe';
import { MatRadioModule } from '@angular/material/radio';


import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar';

// Import routing module
import { AppRoutingModule } from './app-routing.module';

// Import app component
import { AppComponent } from './app.component';

// Import containers
import {
  DefaultFooterComponent,
  DefaultHeaderComponent,
  DefaultLayoutComponent,
} from './containers';

import {
  AvatarModule,
  BadgeModule,
  BreadcrumbModule,
  ButtonGroupModule,
  ButtonModule,
  CardModule,
  DropdownModule,
  FooterModule,
  FormModule,
  GridModule,
  HeaderModule,
  ListGroupModule,
  NavModule,
  ProgressModule,
  SharedModule,
  SidebarModule,
  TabsModule,
  UtilitiesModule,
  ModalModule,


} from '@coreui/angular';

import { IconModule, IconSetService } from '@coreui/icons-angular';
import { LoginComponent } from './pages/login/login.component';
import { ItemCategoryComponent } from './pages/item/item-category/item-category.component';
import { ItemPointComponent } from './pages/item/item-point/item-point.component';
import { UserAddComponent } from './pages/user/user-add/user-add.component';
import { LoadingComComponent } from './pages/shared/loading-com/loading-com.component';
import { SpinnerModule,AlertModule  } from '@coreui/angular';
import { MatCardModule } from '@angular/material/card';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { GroupComponent } from './pages/group/group.component';
import { BuildingComponent } from './pages/building/building.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ConfirmDialogComponent } from './pages/shared/dialog/confirm-dialog/confirm-dialog.component';
import { ItemManageComponent } from './pages/item/item-manage/item-manage.component';
import { ItemDistributeComponent } from './pages/item/item-distribute/item-distribute.component';
import { ItemDialogComponent } from './pages/shared/dialog/item-dialog/item-dialog.component';
import {MatMenuModule} from '@angular/material/menu';
import { CommondialogComponent } from './pages/shared/dialog/commondialog/commondialog.component';
import { ItemdistributeComponent } from './pages/itemdistribute/itemdistribute.component';
import { ItemFindComponent } from './pages/item/item-find/item-find.component';






const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

const APP_CONTAINERS = [
  DefaultFooterComponent,
  DefaultHeaderComponent,
  DefaultLayoutComponent,
];

@NgModule({
  declarations: [AppComponent, ...APP_CONTAINERS,GroupItemsByCategoryPipe,LoginComponent, ItemCategoryComponent, ItemPointComponent, UserAddComponent, LoadingComComponent, GroupComponent, BuildingComponent, ConfirmDialogComponent, ItemManageComponent, ItemDistributeComponent, ItemDialogComponent, CommondialogComponent, ItemdistributeComponent, ItemFindComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AvatarModule,
    BreadcrumbModule,
    FooterModule,
    DropdownModule,
    GridModule,
    HeaderModule,
    SidebarModule,
    IconModule,
    PerfectScrollbarModule,
    NavModule,
    ButtonModule,
    FormModule,
    UtilitiesModule,
    ButtonGroupModule,
    ReactiveFormsModule,
    SidebarModule,
    SharedModule,
    TabsModule,
    ListGroupModule,
    ProgressModule,
    BadgeModule,
    ListGroupModule,
    CardModule,
    SpinnerModule,
    AlertModule,
    FormsModule,
    HttpClientModule,
    ModalModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatGridListModule,
    MatProgressBarModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatDialogModule,
    MatCheckboxModule,
    MatRadioModule,
    MatMenuModule

  ],
  exports: [MatRadioModule],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    IconSetService,
    Title
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
