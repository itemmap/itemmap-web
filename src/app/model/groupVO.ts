export interface GroupVO {
  id: number;
  name: string;
  is_admin_group:number;
}
