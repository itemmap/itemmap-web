import { GroupVO } from "./groupVO";

export interface UserGroupInfoVO {
  id:number
  name: string;
  identity_number:string;
  group_info:GroupVO[];

}
