export interface GroupDistributeItemVO {
  id:number;
  item_id:number;
  grouplist_id: number;
  group_name: string;
  count:number;
}
