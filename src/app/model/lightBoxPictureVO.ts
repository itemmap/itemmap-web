export interface LightBoxPictureVO {
  imageSrc:string;
  imageAlt:string;
}
