export interface ItemVO {
  admin_group_id:number,
  id:number;
  name: string;
  category_id:number;
  category_name:string;
  count:number;
  is_lock:number;
  is_map_point:number;
  is_map_search:number;
  is_number:number;
  created_at:string;


}
