export interface ResponseModel {
  status: string,
  fail: Fail
  result:any
  resultarray:any
}

export interface Fail {
  errorCode: string,
  errorMessage: string,
}
