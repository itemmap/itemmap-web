export interface ItemPointVO {
  groupid:number;
  floor_id:number;
  x:string;
  y:string;
  item_number_id_list:string[]
  item_group_id:number;
  count:number;

}




// "groupid":groupid,
// "floor_id":1
// "x": pointItem.x,
// "y":pointItem.y,
// "item_number_id_list":[],
// "item_group_id":1,
// "count":5
