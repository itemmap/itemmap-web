export interface UserProfileVO {
  class_flag: string;
  class_name: string;
  class_code: string;
  shift_no: string;
  emp_name: string;
  emp_title: string;
  emp_dept: string;
  start_date: string;
  start_time: string;
  end_date: string;
  end_time: string;
  check_in: string;
  check_in_type:string;
  check_out: string;
  check_out_type: string;
  holi_type: string;
  signOutUIOpen: boolean;
}

