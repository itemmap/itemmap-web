export interface GroupItemVO {
  id:number;
  itemCategory:string;
  groupId: number;
  name: string;
  icon:string;
  itemUuid:string;
  count:number;
}
