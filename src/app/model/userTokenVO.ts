import { UserHrInfoVO } from "./userHrInfoVO";

export interface UserToken {
  sub: string;
  role: string;
  userProfile: string;//UserHrInfoVO
  created:string;
  exp: number;
}
