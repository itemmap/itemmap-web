export interface NoNumberItemVO {
  count:number;
  group_name:string;
  grouplist_id:number;
  id:number;
  item_id:number;
  item_name:string;
  item_group_id:number;
  icon:string;
}
