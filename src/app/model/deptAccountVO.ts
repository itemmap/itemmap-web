// public class DeptAccountVO {
//   public String PBIusername;
//   public String name;
//   public String departmentName;
//   public String PBIusername_full;
//   public int isSubGroup;
// }



export interface DeptAccountVO {
  PBIusername: string;
  name: string;
  departmentName: string;
  PBIusername_full: string;
  isSubGroup: number;

}
