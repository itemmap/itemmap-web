import { UserProfileVO } from "./userProfileVO";

export interface UserHrInfoVO {
  authentication:boolean;
  success:boolean;
  result:string;
  data: UserProfileVO;

}
