import { GroupDistributeNumberItemVO } from "./GroupDistributeNumberItemVO";

export interface NumberItemVO {
  unassigned_count:number;
  distribute_count:number;
  item_number_list: GroupDistributeNumberItemVO[];

}
