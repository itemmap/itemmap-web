import { NoNumberItemVO } from "./noNumberItemVO";
import { NeedNumberItemVO } from "./needNumberItemVO";
export interface GroupItemInfoVO {

  item_distribute_group_list:NoNumberItemVO[]
  item_number_list:NeedNumberItemVO[]
}
