export interface AddItemVO {
  admin_group_id:number;
  category_id:number;
  count:number;
  is_lock:number;
  is_map_point:number;
  is_map_search:number;
  is_number:number;
  name: string;


}
