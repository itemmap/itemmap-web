

export interface ItemCategoryVO {
  id:number;
  name: string;
  icon: string;
  action:string;
  is_lock:number;
}
