export interface GroupFloorPointItemVO {
  id:number;
  item_number_id_list: any[];
  item_number_id:string;
  item_group_id:number;
  grouplist_id:number;
  floor_id:number;
  point_x:string;
  point_y:string;
  count:number;
  created_at:string;

}
