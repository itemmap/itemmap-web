export interface FloorVO {
  id: number;
  building_id: number;
  floor_name:string;
  floor_Image_url:string;
}
