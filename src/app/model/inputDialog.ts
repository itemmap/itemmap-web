export interface InputDialog {
  title: string,
  content: string,
  label: string,
  inputResult:string,
}
