import { GroupDistributeItemVO } from 'src/app/model/groupDistributeItemVO';

export interface ItemDistributeVO {

  item_name: string;
  unassigned_count: number;
  distribute_count:number;
  item_distribute_group_list: GroupDistributeItemVO[];
}
