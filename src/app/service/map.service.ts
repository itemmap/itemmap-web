import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UtilsService } from '../util/utils.service';
import { MapInfoVO } from '../model/mapInfoVO';
import { ItemPointVO } from '../model/itemPointVO';



@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private http: HttpClient,private utilsService:UtilsService) { }
  token: string = localStorage.getItem('userToken')!;


  async getGroupMapInfoList(groupId:number) {
    const url = environment.apiEndpoint + "/group/get/floor/"+groupId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url,{ headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.resultarray
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }


  async findGroupItem(groupId:number) {
    const url = environment.apiEndpoint + "/itemgroup/get/"+groupId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url,{ headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }






  saveItemPointInfo(groupid: number, floorid: number, pointItem: ItemPointVO,point_x:number,point_y:number) {
    const url = environment.apiEndpoint + "/itempoint/post";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });

    let body: any = {
      grouplist_id: groupid,
      floor_id: floorid,
      point_x: String(point_x),
      point_y: String(point_y),
      item_number_id_list: null,
      item_group_id: pointItem.item_group_id,
      count: pointItem.count,
    };
    // console.log(body)

    if (pointItem.item_number_id_list.length > 0) {
      body.item_number_id_list = pointItem.item_number_id_list;
      body.item_group_id = null;
      body.count = 1;
    }

    return this.http.post<any>(url, body,{headers})
      .toPromise()
      .then(res => {
        if (res != null) {
          return res;
        }
      });
  }



  async getFloorMapPointInfo(selectedMap:MapInfoVO) {
    const url = environment.apiEndpoint + "/itempoint/get/group/"+selectedMap.group_id+"/floor/"+selectedMap.floor_id;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url,{ headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }




  async deleteFloorMapPointInfo(itemId:number) {
    const url = environment.apiEndpoint + "/itempoint/delete/"+itemId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.delete<any>(url,{ headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x =>
        console.log(x)
       );

  }










}
