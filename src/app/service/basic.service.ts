import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserProfileVO } from '../model/userProfileVO';
import { requestUserProfileVO } from '../model/requestUserProfileVO';
import { UtilsService } from '../util/utils.service';

@Injectable({
  providedIn: 'root',
})
export class BasicService {
  user!: UserProfileVO;
  constructor(private http: HttpClient,private utilsService:UtilsService) {}
  requestUserProfileVO: requestUserProfileVO = {
    account: '',
    password: '',
    project: '',
  };
  getUserProfile(userAccount: string){
    const url = environment.middleWareEndpoint + '/query/his';
    this.requestUserProfileVO.account = userAccount;
    return this.http.post<any>(url,this.requestUserProfileVO)
    .toPromise().then(
      async res => {
        if (res != null) {
          res.data as UserProfileVO;
          return res.data;
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }
}
