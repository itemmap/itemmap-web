import { Pipe, PipeTransform } from '@angular/core';
import { GroupItemVO } from '../model/groupItemVO';

@Pipe({name: 'groupItemsByCategory'})
export class GroupItemsByCategoryPipe implements PipeTransform {
  transform(items: GroupItemVO[]): {[category: string]: GroupItemVO[]} {
    const groupedItems: {[category: string]: GroupItemVO[]} = {};

    for (const item of items) {
      if (!groupedItems[item.itemCategory]) {
        groupedItems[item.itemCategory] = [];
      }

      groupedItems[item.itemCategory].push(item);
    }

    return groupedItems;
  }
}
