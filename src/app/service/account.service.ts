import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AccountVO } from '../model/userGroupVO';
import { FindUserInfoVO } from '../model/findUserInfoVO';
import { MemberVO } from '../model/memberVO';
import { AddMemberVO } from '../model/addMemberVO';
import { UtilsService } from '../util/utils.service';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  accountList: MemberVO[] = [];
  groupUserList:AccountVO[]=[];
  addMemberRequestVO:AddMemberVO={
    name: '',
    identity_number: ''
  }
  token:string = localStorage.getItem('userToken')!;



  constructor(private http: HttpClient,private utilsService:UtilsService) {

  }

  async getAllAccount() {
    const url = environment.apiEndpoint + "/user/get/all";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, {headers})
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.resultarray
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }

  async getGroupAccount(groupId: number): Promise<AccountVO[]> {
    const url = environment.apiEndpoint + '/group/get/' + groupId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          return res.result.userList;
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }

 async addAccount(findUserInfoVO: FindUserInfoVO) {
    const url = environment.apiEndpoint + '/user/post';
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    this.addMemberRequestVO.name = findUserInfoVO.name;
    this.addMemberRequestVO.identity_number = findUserInfoVO.id_number;
    return this.http.post<any>(url, this.addMemberRequestVO, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          res = '新增用戶成功'
          return res;
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }


  async getAccountAllGroupList(userAccount:string) {
    const url = environment.apiEndpoint + "/user/post/finduser";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    var body={
      "identity_number":userAccount
    }
    return this.http.post<any>(url,body, {headers})
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      );

  }


  async addUserToLDAP(account: string, name: string, description: string) {
    // const url = environment.apiEndpoint + "/auth/addaccount";
    const url = environment.apiEndpoint+"/auth/addaccount"
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    var body = {
      "account": account,
      "name": name,
      "description": description,
      "project":"itemmap"
    }

    return this.http.post<any>(url, body,{headers})
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      );

  }





}
