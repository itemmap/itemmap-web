import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../model/responseModel';
import { ResponseDef } from '../constant/ResponseDef';
import { BuildingVO } from '../model/buildingVO';
import { FloorVO } from '../model/floorVO';
import { GroupVO } from 'src/app/model/groupVO';
import { UtilsService } from '../util/utils.service';
@Injectable({
  providedIn: 'root'
})
export class BuildingService {
  token:string = localStorage.getItem('userToken')!;
  buildingList!:BuildingVO[];
  floorList!:FloorVO[];
  groupList!:GroupVO[];
  constructor(private http: HttpClient,private utilsService:UtilsService) { }


  async getAllBuilding():Promise<BuildingVO[]>{
    const url = environment.apiEndpoint + "/building/get/all";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<ResponseModel>(url, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          return res.resultarray
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }


  async getBuildingFloorList(buildingId: number):Promise<FloorVO[]>{
    const url = environment.apiEndpoint + '/floor/get/' + buildingId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<ResponseModel>(url, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          return res.result
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }


  async getFloorGroupList(floorId: number):Promise<GroupVO[]>{
    const url = environment.apiEndpoint + '/floor/get/groupname/' + floorId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<ResponseModel>(url, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          return res.result
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }







  async getGroupBuildingList() {
    const url = environment.jsonDataTestEndpoint + "/userMapInfo";
    // const headers = new HttpHeaders({
    //   'Authorization': `Bearer ${this.token}`
    // });
    return this.http.get<any>(url)
      .toPromise().then(
        async res => {
          if (res != null) {
            return res
          }
        }
      ).catch();

  }

}
