import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../model/responseModel';
import { ResponseDef } from '../constant/ResponseDef';
import { ItemCategoryVO } from '../model/itemCategoryVO';
import { AddItemCategoryVO } from '../model/addItemCategoryVO';
import { UtilsService } from '../util/utils.service';
import { AddItemVO } from '../model/addItemVO';
import { GroupAddItemVO } from '../model/groupAddItemVO';
import { UpdateItemVO } from '../model/updateItemVO';


@Injectable({
  providedIn: 'root'
})
export class ItemService {
  itemCategoryList!: ItemCategoryVO[];
  token: string = localStorage.getItem('userToken')!;

  constructor(private http: HttpClient, private utilsService: UtilsService) { }

  async getAllItemcategory() {
    const url = environment.apiEndpoint + "/itemcategory/get/all";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.resultarray
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }



  async getFloorPointedItem(floorId:number) {
    const url = environment.apiEndpoint + "/itempoint/get/floor/"+floorId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }










  async getAdminItemList(adminGroupId:number){
    const url = environment.apiEndpoint + "/item/get/admin/"+adminGroupId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));
  }



  async addItemcategory(addItemCategoryVO: AddItemCategoryVO) {
    const url = environment.apiEndpoint + "/itemcategory/post";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(url, addItemCategoryVO, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));
  }

  async updateItem(updateItem:UpdateItemVO,itemId:number) {
    const url = environment.apiEndpoint + "/item/put/"+itemId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.put<any>(url, updateItem,{ headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }



  async getAllItem() {
    const url = environment.apiEndpoint + "/item/get/all";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.resultarray
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }

  async addItem(addItemVO: AddItemVO) {
    const url = environment.apiEndpoint + "/item/post";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(url, addItemVO, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }

  async deleteItem(itemId: number) {
    const url = environment.apiEndpoint + "/item/delete/" + itemId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.delete<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }



  async deleteItemCategory(itemCategoryUuid: number) {
    const url = environment.apiEndpoint + "/itemcategory/delete/" + itemCategoryUuid;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.delete<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }



  async deleteNumberItem(itemUuId: number) {
    const url = environment.apiEndpoint + "/item/number/delete/" + itemUuId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.delete<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }

  async deleteDistributeItem(itemGroupid: number) {
    const url = environment.apiEndpoint + "/item/delete/distribute/" + itemGroupid;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.delete<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }





  async updateItemCount(itemGroupId: number, updateCount: number) {
    const url = environment.apiEndpoint + "/item/put/distribute/" + itemGroupId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    var body = {
      "count": updateCount
    }
    return this.http.put<any>(url, body, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x =>
        this.utilsService.handleAuthError(x));

  }





  async getItemInfo(itemId: number) {
    const url = environment.apiEndpoint + "/item/group/get/" + itemId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }

  async getNumberItemInfo(itemId: number) {
    const url = environment.apiEndpoint + "/item/number/get/" + itemId;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }




  async distributeToGroup(groupAddItemVO: GroupAddItemVO, itemType: boolean) {
    var url = '';
    if (itemType == true) {
      url = environment.apiEndpoint + "/item/post/distribute/number";
    }
    if (itemType == false) {
      url = environment.apiEndpoint + "/item/post/distribute";
    }
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(url, groupAddItemVO, { headers })
      .toPromise().then(
        async res => {
          if (res != null) {
            return res.result
          }
        }
      ).catch(x => this.utilsService.handleAuthError(x));

  }











}
