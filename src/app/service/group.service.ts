import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../model/responseModel';
import { ResponseDef } from '../constant/ResponseDef';
import { UserGroupVO } from '../model/userGroupVO';
import { GroupAddMemberRequestVO } from '../model/group-addMember-requestVO';
import { RemoveMemberRequestVO } from '../model/removeMember-requestVO';
import { UtilsService } from '../util/utils.service';




@Injectable({
  providedIn: 'root'
})
export class GroupService {
  userGroupVO!: UserGroupVO[];
  token:string = localStorage.getItem('userToken')!;
  constructor(private http: HttpClient,private utilsService:UtilsService) { }



  async getAllGroupList() {
    const url = environment.apiEndpoint + "/group/get/all";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          return res.resultarray;
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }


  async getGroupList() {
    const url = environment.apiEndpoint + "/group/get/usergroup";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(url, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          return res.resultarray;
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }

  async addMember(addMemberRequestVO: GroupAddMemberRequestVO) {
    const url = environment.apiEndpoint + "/group/post/usergroup";
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(url, addMemberRequestVO,{headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          res = '新增用戶至群組成功'
          return res;
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }

  async removeMember(removeMemberRequestVO: RemoveMemberRequestVO) {
    const url = environment.apiEndpoint + "/group/delete/"+removeMemberRequestVO.groupid+"/"+removeMemberRequestVO.userid;
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.delete<any>(url, {headers})
    .toPromise().then(
      async res => {
        if (res != null) {
          res = '群組刪除用戶成功'
          return res;
        }
      }
    ).catch(x => this.utilsService.handleAuthError(x));
  }










}
