export class MessageMapCode {
  public errorCode!: string;
  public errorMessage!: string;

  constructor(errorCode:string, errorMessage:string){
    this.errorCode = errorCode;
    this.errorMessage= errorMessage;
  }
}

export class ResponseDef {
  public static successString = "SUCCESS";
  public static failString = "FAIL";

  public static success = "S0000";
  public static defaulErrorMessge = "未知錯誤";
  
  static ErrorDef:Map<string, MessageMapCode> = new Map(
    [
      ["E0000", new MessageMapCode("E0000","未知錯誤")],
      ["E0001", new MessageMapCode("E0000","查無資料")],
      ["E2001", new MessageMapCode("E4001","密碼為必填")],
      ["E4001", new MessageMapCode("E4001","密碼錯誤")],
      ["E4002", new MessageMapCode("E4002","資料庫操作異常，請重新嘗試")],
      ["E4003", new MessageMapCode("E4003","此身份證字號已存在，請重新確認")],
      ["E4004", new MessageMapCode("E4004","用戶不存在")],
      ["E4005", new MessageMapCode("E4005","QRCode過期需重新產生")],
      ["E4008", new MessageMapCode("E4008","案件不存在")],
      ["E4009", new MessageMapCode("E4009","傳送中，不可進行其他操作")],
      ["E4010", new MessageMapCode("E4010","案件已被作廢，不可進行其他操作")],
      ["E4011", new MessageMapCode("E4011","案件狀態非新增案件，不可進行其他操作")],
      ["E4018", new MessageMapCode("E4018","帳號密碼驗證失敗")],
      ["E4019", new MessageMapCode("E4019","權限不足")],
      ["E4020", new MessageMapCode("E4020","系統已有相同案件，不允許重複新增")],
    ]
  );
}


